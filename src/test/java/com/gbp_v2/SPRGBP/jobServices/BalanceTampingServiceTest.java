package com.gbp_v2.SPRGBP.jobServices;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.dto.BalanceTampingStatDTO;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BalanceTampingServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(BalanceTampingServiceTest.class);

    @Mock
    private BankAccountRepository bankAccountRepository;

    @InjectMocks
    private BalanceTampingService balanceTampingService;

    @Test
    public void testGenerateBalanceTampingStats() {
        Long bankAccountId = 1L;

        // Mocking BankAccount
        BankAccount bankAccount = new BankAccount();
        bankAccount.setInitialBalance(new BigDecimal("1000.0"));
        when(bankAccountRepository.findById(bankAccountId)).thenReturn(Optional.of(bankAccount));

        // Mocking transaction statistics
        List<Object[]> totalDebCreStatResults = Arrays.asList(
                new Object[]{2021, 1, BigDecimal.valueOf(200.0), BigDecimal.valueOf(300.0)},
                new Object[]{2021, 2, BigDecimal.valueOf(150.0), BigDecimal.valueOf(400.0)}
        );

        List<BalanceTampingStatDTO> result = balanceTampingService.generateBalanceTampingStats(bankAccountId, totalDebCreStatResults);

        // Log the result for debugging
        result.forEach(dto -> logger.debug("Result DTO: {}", dto));

        assertEquals(2, result.size());

        // Expected results
        BalanceTampingStatDTO expectedDto1 = new BalanceTampingStatDTO();
        expectedDto1.setYear(2021);
        expectedDto1.setMonth(1);
        expectedDto1.setTotalDebit(200.0);
        expectedDto1.setTotalCredit(300.0);
        expectedDto1.setTampingBalance(1100.0);

        BalanceTampingStatDTO expectedDto2 = new BalanceTampingStatDTO();
        expectedDto2.setYear(2021);
        expectedDto2.setMonth(2);
        expectedDto2.setTotalDebit(150.0);
        expectedDto2.setTotalCredit(400.0);
        expectedDto2.setTampingBalance(1350.0);

        // Log the expected DTOs
        logger.debug("Expected DTO1: {}", expectedDto1);
        logger.debug("Expected DTO2: {}", expectedDto2);

        // Comparing objects directly
        assertEquals(expectedDto1, result.get(0));
        assertEquals(expectedDto2, result.get(1));
    }
}
