package com.gbp_v2.SPRGBP.jobServices;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.dto.PercentageCatStatDTO;
import com.gbp_v2.SPRGBP.entities.list.CategoryName;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PercentagePerCategoryServiceTest {

    @Mock
    private BankAccountRepository bankAccountRepository;

    @InjectMocks
    private PercentagePerCategoryService percentagePerCategoryService;

    @Test
    public void testCalculatePercentagePerCategory() {
        Long bankAccountId = 1L;

        // Mocking BankAccount
        BankAccount bankAccount = new BankAccount();
        bankAccount.setInitialBalance(new BigDecimal("1000.0"));
        when(bankAccountRepository.findById(bankAccountId)).thenReturn(Optional.of(bankAccount));

        // Mocking transaction statistics
        List<Object[]> totalDebCreStatResults = Arrays.asList(
                new Object[]{2021, 1, BigDecimal.valueOf(200.0), BigDecimal.valueOf(300.0)},
                new Object[]{2021, 2, BigDecimal.valueOf(150.0), BigDecimal.valueOf(400.0)}
        );

        // Utilisation de CategoryName au lieu de String
        List<Object[]> totalCatDebCreStatResults = Arrays.asList(
                new Object[]{2021, 1, CategoryName.CATEGORY1, BigDecimal.valueOf(100.0), BigDecimal.valueOf(150.0)},
                new Object[]{2021, 2, CategoryName.CATEGORY2, BigDecimal.valueOf(50.0), BigDecimal.valueOf(250.0)}
        );

        List<PercentageCatStatDTO> result = percentagePerCategoryService.calculatePercentagePerCategory(bankAccountId, totalCatDebCreStatResults, totalDebCreStatResults);

        assertEquals(2, result.size());

        // Further assertions based on the expected result
    }
}
