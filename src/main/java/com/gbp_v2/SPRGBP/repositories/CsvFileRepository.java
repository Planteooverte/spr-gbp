package com.gbp_v2.SPRGBP.repositories;

import com.gbp_v2.SPRGBP.entities.model.CsvFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CsvFileRepository extends JpaRepository<CsvFile, Long> {

    @Query("SELECT cf FROM CsvFile cf " +
            "INNER JOIN cf.bankAccount ba " +
            "INNER JOIN ba.customer c " +
            "WHERE c.id = :customerId")
    List<CsvFile> findByCustomerId(Long customerId);

    @Query("SELECT COUNT(cf) > 0 FROM CsvFile cf " +
            "WHERE cf.bankAccount.id = :bankAccountId AND cf.fileName = :fileName")
    boolean existsByBankAccountIdAndFileName(Long bankAccountId, String fileName);

    Optional<CsvFile> findByBankAccountIdAndFileName(Long bankAccountId, String fileName);

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM CsvFile c WHERE c.id = :csvFileId AND c.bankAccount.id = :bankAccountId")
    boolean existsByIdAndBankAccountId(Long csvFileId, Long bankAccountId);
}
