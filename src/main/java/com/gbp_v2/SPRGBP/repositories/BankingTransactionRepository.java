package com.gbp_v2.SPRGBP.repositories;

import com.gbp_v2.SPRGBP.entities.model.BankingTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankingTransactionRepository extends JpaRepository<BankingTransaction, Long> {

    // Find all banking transactions by CSV file ID
    @Query("SELECT bt FROM BankingTransaction bt WHERE bt.csvFile.id = :csvFileId")
    List<BankingTransaction> findByCsvFileId(@Param("csvFileId") Long csvFileId);

    // Get list of banking transaction by bankaccountId
    List<BankingTransaction> findByBankAccountId(Long bankAccount_id);

    // STATISTICAL SYSTEM: Total sum of expenses, credits and balances per month and per year on the bankAccountId
    @Query("SELECT EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date), SUM(bt.debit), SUM(bt.credit) " +
            "FROM BankingTransaction bt " +
            "WHERE bt.bankAccount.id = :bankAccountId " +
            "GROUP BY EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date) " +
            "ORDER BY EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date)")
    List<Object[]> getTotalDebitCreditStatPerMonthAndYear(Long bankAccountId);

    // STATISTICAL SYSTEM: Percentage of Category in credit and debit per month and per year on bankAccountId
    @Query("SELECT EXTRACT(YEAR FROM bt.date) AS year, " +
            "EXTRACT(MONTH FROM bt.date) AS month, " +
            "c.categoryName, " +
            "SUM(bt.credit) AS totalCatCredit, " +
            "SUM(bt.debit) AS totalCatDebit " +
            "FROM BankingTransaction bt " +
            "JOIN bt.category c " +
            "WHERE bt.bankAccount.id = :bankAccountId " +
            "GROUP BY EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date), c.categoryName " +
            "ORDER BY EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date), c.categoryName")
    List<Object[]> getTotalDebitCreditStatPerCategoryAndMonthAndYear(@Param("bankAccountId") Long bankAccountId);

    // STATISTICAL SYSTEM: Get the number of transactions per month and per year
    @Query("SELECT EXTRACT(YEAR FROM bt.date) AS year, " +
            "EXTRACT(MONTH FROM bt.date) AS month, " +
            "COUNT(bt) AS transactionCount " +
            "FROM BankingTransaction bt " +
            "WHERE bt.bankAccount.id = :bankAccountId " +
            "GROUP BY EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date) " +
            "ORDER BY EXTRACT(YEAR FROM bt.date), EXTRACT(MONTH FROM bt.date)")
    List<Object[]> getTransactionCountPerMonthAndYear(@Param("bankAccountId") Long bankAccountId);
}
