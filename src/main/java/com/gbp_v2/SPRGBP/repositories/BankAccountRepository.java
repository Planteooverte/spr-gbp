package com.gbp_v2.SPRGBP.repositories;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    List<BankAccount> findByCustomerId(Long customerId);
    boolean existsByReferenceAccount(String referenceAccount);
    Optional<BankAccount> findByReferenceAccount(String referenceAccount);

    @Query("SELECT CASE WHEN COUNT(b) > 0 THEN true ELSE false END FROM BankAccount b WHERE b.id = :bankAccountId AND b.customer.id = :customerId")
    boolean existsByIdAndCustomerId(Long bankAccountId, Long customerId);
}
