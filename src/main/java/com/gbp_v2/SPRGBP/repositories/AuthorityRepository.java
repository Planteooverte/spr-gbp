package com.gbp_v2.SPRGBP.repositories;

import com.gbp_v2.SPRGBP.entities.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
