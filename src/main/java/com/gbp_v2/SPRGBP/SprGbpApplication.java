package com.gbp_v2.SPRGBP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity(debug = true)
public class SprGbpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprGbpApplication.class, args);
	}

}
