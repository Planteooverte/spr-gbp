package com.gbp_v2.SPRGBP.mailServices;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sibApi.TransactionalEmailsApi;
import sibModel.SendSmtpEmail;
import sibModel.SendSmtpEmailTo;
import sibModel.SendSmtpEmailSender;
import sibModel.SendSmtpEmailAttachment;
import sendinblue.ApiClient;
import sendinblue.ApiException;
import sendinblue.Configuration;
import sendinblue.auth.ApiKeyAuth;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class MailService {

    private static final Logger logger = LoggerFactory.getLogger(MailService.class);

    private final String apiKey;

    public MailService(@Value("${sendinblue.apiKey}") String apiKey) {
        this.apiKey = apiKey;
    }

    public void sendEmail(String toEmail, String subject, String content, byte[] imageBytes, String imageName) throws ApiException {
        // Configurer le client SendinBlue avec l'API Key
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        ApiKeyAuth apiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("api-key");
        apiKeyAuth.setApiKey(apiKey);

        // Créer l'instance de l'API pour envoyer des e-mails transactionnels
        TransactionalEmailsApi apiInstance = new TransactionalEmailsApi();

        // Configurer l'e-mail
        SendSmtpEmailSender sender = new SendSmtpEmailSender()
                .email("noreply.app.gbp@gmail.com")
                .name("GBP Team");
        SendSmtpEmailTo to = new SendSmtpEmailTo().email(toEmail);
        List<SendSmtpEmailTo> toList = new ArrayList<>();
        toList.add(to);

        SendSmtpEmail sendSmtpEmail = new SendSmtpEmail()
                .sender(sender)
                .to(toList)
                .subject(subject)
                .htmlContent(content);

        // Ajouter l'image en tant que pièce jointe
        if (imageBytes != null) {
            SendSmtpEmailAttachment attachment = new SendSmtpEmailAttachment();
            attachment.setName(imageName);
            attachment.setContent(Base64.getEncoder().encodeToString(imageBytes).getBytes());

            sendSmtpEmail.addAttachmentItem(attachment);
        }

        // Envoyer l'e-mail
        try {
            apiInstance.sendTransacEmail(sendSmtpEmail);
            logger.info("Email sent successfully to: {}", toEmail);
        } catch (ApiException e) {
            logger.error("Failed to send email to: {}. Error: {}", toEmail, e.getMessage());
            throw e;
        }
    }
}
