package com.gbp_v2.SPRGBP.config;

public interface JWTConfig {
    public static final String JWT_KEY = "jxgEQeXHuPq8VdbyYFNkANdudQ53YUn4";
    public static final String JWT_HEADER = "Authorization";
    public static final long JWT_EXPIRATION = 1800000; // 30 minutes en millisecondes
}
