package com.gbp_v2.SPRGBP.config;

import com.gbp_v2.SPRGBP.filters.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.*;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import com.gbp_v2.SPRGBP.securityServices.CustomAuthenticationEntryPoint;

import java.util.Arrays;
import java.util.Collections;
import java.util.function.Supplier;

@Configuration
public class WebSecurityConfig {

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // Custom CsrfTokenRequestHandler for SPA
        CsrfTokenRequestHandler requestHandler = new SpaCsrfTokenRequestHandler();

        http
            // Disable JSESSIONID and configure stateless session management
            .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
            // Configure CORS
            .cors(corsCustomizer -> corsCustomizer.configurationSource(new CorsConfigurationSource() {
                @Override
                public CorsConfiguration getCorsConfiguration(HttpServletRequest request) {
                    CorsConfiguration config = new CorsConfiguration();
                    config.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
                    config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
                    config.setAllowCredentials(true);
                    config.setAllowedHeaders(Collections.singletonList("*"));
                    config.setExposedHeaders(Arrays.asList("Authorization"));
                    config.setMaxAge(3600L);
                    return config;
                }
            }))
            // CSRF configuration
            //.csrf(csrf -> csrf.disable())
            .csrf((csrf) -> csrf
                    .csrfTokenRepository(new HttpSessionCsrfTokenRepository())
                    .csrfTokenRequestHandler(requestHandler)
                )
                //   .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                //   .ignoringRequestMatchers("/secretcode", "/register", "/passwordForgotten"))

            // Add filters
            .addFilterAfter(new CsrfTokenFilter(), BasicAuthenticationFilter.class)
            .addFilterBefore(new RegistrationRequestFilter(), BasicAuthenticationFilter.class)
            .addFilterAfter(new LogUserConnexion(), BasicAuthenticationFilter.class)
            .addFilterAfter(new JWTokenGeneratorFilter(), BasicAuthenticationFilter.class)
            .addFilterBefore(new JWTokenValidatorFilter(), BasicAuthenticationFilter.class)
            .authorizeHttpRequests((requests) -> requests
                    .requestMatchers("/registerNewUser", "/forgottenPassword").permitAll()
                    .requestMatchers("/customer", "/secretcode" ).permitAll()
                    .requestMatchers("/disconnect", "/updateProfil", "/deleteUser").authenticated()
                    .requestMatchers("/bankaccount", "/bankaccount/**").authenticated()
                    .requestMatchers("/csvfile", "/csvfile/**").authenticated()
                    .requestMatchers("/bankingtransaction", "/category", "/statistical").authenticated()
                    .requestMatchers("/error").permitAll() // à désactiver en fin de développement
            )
            .formLogin(Customizer.withDefaults())
            .httpBasic(Customizer.withDefaults())
            .exceptionHandling(exceptionHandling -> exceptionHandling
                    .authenticationEntryPoint(new CustomAuthenticationEntryPoint()) // Configurer CustomAuthenticationEntryPoint
            );

        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // Custom SpaCsrfTokenRequestHandler
    public static class SpaCsrfTokenRequestHandler implements CsrfTokenRequestHandler {
        private final CsrfTokenRequestHandler delegate = new XorCsrfTokenRequestAttributeHandler();

        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response, Supplier<CsrfToken> csrfToken) {
            delegate.handle(request, response, csrfToken);
        }

        @Override
        public String resolveCsrfTokenValue(HttpServletRequest request, CsrfToken csrfToken) {
            String headerToken = request.getHeader(csrfToken.getHeaderName());
            return (StringUtils.hasText(headerToken)) ? headerToken : delegate.resolveCsrfTokenValue(request, csrfToken);
        }
    }
}
