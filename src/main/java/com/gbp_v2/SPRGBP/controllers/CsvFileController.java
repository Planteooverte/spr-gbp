package com.gbp_v2.SPRGBP.controllers;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.model.BankingTransaction;
import com.gbp_v2.SPRGBP.entities.model.CsvFile;
import com.gbp_v2.SPRGBP.entities.model.Customer;
import com.gbp_v2.SPRGBP.entities.dto.CsvFileDTO;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import com.gbp_v2.SPRGBP.repositories.BankingTransactionRepository;
import com.gbp_v2.SPRGBP.repositories.CsvFileRepository;
import com.gbp_v2.SPRGBP.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import com.gbp_v2.SPRGBP.entities.dto.LongCsvFileDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/csvfile")

public class CsvFileController {

    private static final Logger logger = LoggerFactory.getLogger(CsvFileController.class);
    @Autowired
    private CsvFileRepository csvFileRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankingTransactionRepository bankingTransactionRepository;

    @GetMapping()       //METHOD - GET CSVFILE BY USER//
    public ResponseEntity<List<LongCsvFileDTO>> getCsvFilesByUserId(@RequestParam Long id) {
        List<CsvFile> csvFiles = csvFileRepository.findByCustomerId(id);
        if (csvFiles != null && !csvFiles.isEmpty()) {
            List<LongCsvFileDTO> csvFileDTOs = csvFiles.stream().map(csvFile -> new LongCsvFileDTO(
                    csvFile.getId(),
                    csvFile.getFileName(),
                    csvFile.getCreationDate(),
                    csvFile.getUpdateDate(),
                    csvFile.getBankAccount().getId(),
                    csvFile.getBankAccount().getBankName(),
                    csvFile.getBankAccount().getReferenceAccount(),
                    csvFile.getBankAccount().getTypeAccount()
            )).collect(Collectors.toList());
            return ResponseEntity.ok(csvFileDTOs);
        } else if (csvFiles == null || csvFiles.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PostMapping()      //METHOD - CREATE CSVFILE//
    public ResponseEntity<?> addCsvFile(@RequestBody CsvFile csvFile) {
        try {
            // Step 1: Retrieve the authenticated user's email from the principal
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = (String) authentication.getPrincipal();

            // Step 2: Find the customer by email
            List<Customer> customers = customerRepository.findByEmail(username);
            if (customers.isEmpty()) {
                logger.error("No customer found with email: {}", username);
                return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
            }
            Customer customer = customers.get(0);

            Long bankAccountId = csvFile.getBankAccount().getId();
            String fileName = csvFile.getFileName();

            logger.info("Customer ID: {}", customer.getId());
            logger.info("BankAccount ID: {}", bankAccountId);
            logger.info("FileName: {}", fileName);

            // Step 3: Verify that the bankAccountId belongs to the customer
            BankAccount bankAccount = bankAccountRepository.findById(bankAccountId)
                    .orElseThrow(() -> new RuntimeException("Bank account not found"));

            if (!bankAccount.getCustomer().getId().equals(customer.getId())) {
                logger.error("Unauthorized access: Customer ID {} does not own BankAccount ID {}", customer.getId(), bankAccountId);
                return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
            }

            // Step 4: Verify that the fileName does not exist for this bankAccountId
            boolean exists = csvFileRepository.existsByBankAccountIdAndFileName(bankAccountId, fileName);
            if (exists) {
                logger.error("File name already exists for this bank account: {}", fileName);
                return new ResponseEntity<>("Request denied, file name already exists for this bank account", HttpStatus.CONFLICT);
            }

            // Step 6: Save the CsvFile object to the database
            CsvFile createdCsvFile = csvFileRepository.save(csvFile);
            logger.info("CsvFile created with ID: {}", createdCsvFile.getId());

            // Step 7: Create CsvFileDTO from created CsvFile object
            CsvFileDTO csvFileDTO = new CsvFileDTO(
                    createdCsvFile.getId(),
                    createdCsvFile.getFileName(),
                    createdCsvFile.getCreationDate(),
                    createdCsvFile.getUpdateDate(),
                    createdCsvFile.getBankAccount().getId()
            );

            // Step 8: Return a 200 OK response with the created CsvFileDTO object
            return new ResponseEntity<>(csvFileDTO, HttpStatus.CREATED);
        } catch (RuntimeException e) {
            logger.error("Runtime exception: {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping()       //METHODE - UPDATE CSVFILE//
    public ResponseEntity<?> updateCsvFile(@RequestBody CsvFile csvFile) {
        try {
            // Step 1: Retrieve the authenticated user's email from the principal
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = (String) authentication.getPrincipal();

            // Step 2: Find the customer by email
            List<Customer> customers = customerRepository.findByEmail(username);
            if (customers.isEmpty()) {
                logger.error("No customer found with email: {}", username);
                return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
            }
            Customer customer = customers.get(0);

            Long newBankAccountId  = csvFile.getBankAccount().getId();
            String fileName = csvFile.getFileName();

            logger.info("Customer ID: {}", customer.getId());
            logger.info("BankAccount ID: {}", newBankAccountId );
            logger.info("FileName: {}", fileName);

            // Step 3: Verify that the bankAccountId belongs to the customer
            BankAccount bankAccount = bankAccountRepository.findById(newBankAccountId )
                .orElseThrow(() -> new RuntimeException("Bank account not found"));

            if (!bankAccount.getCustomer().getId().equals(customer.getId())) {
                logger.error("Unauthorized access: Customer ID {} does not own BankAccount ID {}", customer.getId(), newBankAccountId );
                return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
            }

            // Step 4: Check for file name uniqueness within the same bank account
            Optional<CsvFile> existingFileWithSameName = csvFileRepository.findByBankAccountIdAndFileName(newBankAccountId , fileName);
            if (existingFileWithSameName.isPresent() && !existingFileWithSameName.get().getId().equals(csvFile.getId())) {
                logger.error("A file with the name '{}' already exists for BankAccount ID {}", fileName, newBankAccountId );
                return new ResponseEntity<>("A file with this name already exists for the given bank account", HttpStatus.CONFLICT);
            }

            // Step 5: Retrieve the existing CsvFile from the database
            CsvFile existingCsvFile = csvFileRepository.findById(csvFile.getId())
                    .orElseThrow(() -> new RuntimeException("CSV file not found"));

            Long oldBankAccountId = existingCsvFile.getBankAccount().getId();

            // Preserve creationDate and update other fields
            existingCsvFile.setFileName(csvFile.getFileName());
            existingCsvFile.setUpdateDate(csvFile.getUpdateDate() != null ? csvFile.getUpdateDate() : new Date());
            existingCsvFile.setBankAccount(bankAccount);

            // Step 6: Save the updated CsvFile object to the database
            CsvFile updatedCsvFile = csvFileRepository.save(existingCsvFile);
            logger.info("CsvFile updated with ID: {}", updatedCsvFile.getId());

            // Step 7: Update banking transactions with the new bankAccountId if necessary
            if (!oldBankAccountId.equals(newBankAccountId)) {
                List<BankingTransaction> transactionsToUpdate = bankingTransactionRepository.findByCsvFileId(csvFile.getId());
                for (BankingTransaction transaction : transactionsToUpdate) {
                    transaction.setBankAccount(bankAccount);
                }
                bankingTransactionRepository.saveAll(transactionsToUpdate);
            }

            return new ResponseEntity<>("csvFile updated", HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error updating CSV file", e);
            return new ResponseEntity<>("An error occurred while updating the CSV file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")     //METHODE - DELETE CSVFILE//
    public ResponseEntity<Object> deleteCsvFile(@PathVariable Long id) {
        logger.info("Request received to delete csvFile with ID: {}", id);

        // Étape : Suppression du CsvFile
        Optional<CsvFile> csvFileToDelete = csvFileRepository.findById(id);
        if (csvFileToDelete.isPresent()) {
            csvFileRepository.deleteById(id);
            logger.info("CsvFile with ID: {} successfully deleted", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            logger.warn("CsvFile with ID: {} not found", id);
            return new ResponseEntity<>("File not found", HttpStatus.NOT_FOUND);
        }
    }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
