package com.gbp_v2.SPRGBP.controllers;

import ch.qos.logback.classic.Logger;
import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.model.Customer;
import com.gbp_v2.SPRGBP.entities.list.TypeAccount;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import com.gbp_v2.SPRGBP.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.LoggerFactory;
import com.gbp_v2.SPRGBP.entities.dto.BankAccountDTO;
import com.gbp_v2.SPRGBP.entities.mapper.BankAccountMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/bankaccount")

public class BankAccountController {

    @Autowired
    private BankAccountRepository bankAccountRepository;
    @Autowired
    private CustomerRepository customerRepository;
    private static final Logger logger = (Logger) LoggerFactory.getLogger(BankAccountController.class);

    @GetMapping()       //METHOD - GET BANKACCOUNT BY USER//
    public List<BankAccount> getBankAccountsByUserId(@RequestParam Long id) {
        List<BankAccount> bankAccounts = bankAccountRepository.findByCustomerId(id);
        if (bankAccounts != null ) {
            return bankAccounts;
        }else {
            return null;
        }
    }

    @GetMapping("/list")  // METHOD - GET BANKACCOUNT BY USER //
    public ResponseEntity<List<BankAccountDTO>> getShortListBankAccountsByUserId(@RequestParam Long id) {
        List<BankAccount> bankAccounts = bankAccountRepository.findByCustomerId(id);
        if (bankAccounts != null && !bankAccounts.isEmpty()) {
            List<BankAccountDTO> bankAccountDTOs = bankAccounts.stream()
                    .map(BankAccountMapper::toDTO)
                    .collect(Collectors.toList());
            return ResponseEntity.ok(bankAccountDTOs);
        } else if (bankAccounts == null || bankAccounts.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PostMapping()      //METHOD - CREATE BANKACCOUNT//
    public ResponseEntity<?> addBankAccount(@RequestBody BankAccountDTO  bankAccountDTO) {
        try {
            // Set the customer ID directly from DTO
            Customer customer = new Customer();
            customer.setId(bankAccountDTO.getCustomerId());

            // Create bankAccount with DTO
            BankAccount bankAccount = new BankAccount();
            bankAccount.setReferenceAccount(bankAccountDTO.getReferenceAccount());
            bankAccount.setBankName(bankAccountDTO.getBankName());
            bankAccount.setAddress(bankAccountDTO.getAddress());
            bankAccount.setCity(bankAccountDTO.getCity());
            bankAccount.setTypeAccount(TypeAccount.valueOf(bankAccountDTO.getTypeAccount()));
            bankAccount.setInitialBalance(bankAccountDTO.getInitialBalance());
            bankAccount.setCustomer(customer);

            // Save bankaccount
            BankAccount createdBankAccount = bankAccountRepository.save(bankAccount);

            // Convert a response DTO
            BankAccountDTO responseDTO = BankAccountMapper.toDTO(createdBankAccount);

            return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("Reference account already exists", HttpStatus.CONFLICT);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping()       //METHODE - UPDATE BANKACCOUNT//
    public ResponseEntity<?> updateBankAccount(@RequestBody BankAccountDTO bankAccountDTO) {
        try {
            // Check - bankaccount exists
            BankAccount existingBankAccount = bankAccountRepository.findById(bankAccountDTO.getId())
                    .orElseThrow(() -> {
                        logger.warn("Bank account not found for update request with ID: {}", bankAccountDTO.getId());
                        return new RuntimeException("Bank account not found");
                    });

            // check - customer exists and he's owner of bankaccount
            if (!existingBankAccount.getCustomer().getId().equals(bankAccountDTO.getCustomerId())) {
                logger.warn("Unauthorized request to update bank account with ID: {}", bankAccountDTO.getId());
                return new ResponseEntity<>("Unauthorized request", HttpStatus.UNAUTHORIZED);
            }

            // check - uniqueness of referenceAccount
            Optional<BankAccount> bankAccountWithSameReference = bankAccountRepository.findByReferenceAccount(bankAccountDTO.getReferenceAccount());
            if (bankAccountWithSameReference.isPresent() && !bankAccountWithSameReference.get().getId().equals(bankAccountDTO.getId())) {
                logger.warn("Reference account already exists for update request with ID: {}", bankAccountDTO.getId());
                return new ResponseEntity<>("Reference account already exists", HttpStatus.CONFLICT);
            }

            // update propreties bankaccount
            existingBankAccount.setBankName(bankAccountDTO.getBankName());
            existingBankAccount.setReferenceAccount(bankAccountDTO.getReferenceAccount());
            existingBankAccount.setAddress(bankAccountDTO.getAddress());
            existingBankAccount.setCity(bankAccountDTO.getCity());
            existingBankAccount.setTypeAccount(TypeAccount.valueOf(bankAccountDTO.getTypeAccount()));
            existingBankAccount.setInitialBalance(bankAccountDTO.getInitialBalance());

            // save bankaccount
            BankAccount updatedBankAccount = bankAccountRepository.save(existingBankAccount);
            logger.info("Bank account successfully updated with ID: {}", bankAccountDTO.getId());
            BankAccountDTO responseDTO = BankAccountMapper.toDTO(updatedBankAccount);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        } catch (DataIntegrityViolationException e) {
            logger.error("Data integrity violation while updating bank account with ID: {}", bankAccountDTO.getId(), e);
            return new ResponseEntity<>("Reference account already exists", HttpStatus.CONFLICT);
        } catch (RuntimeException e) {
            logger.error("Runtime exception while updating bank account with ID: {}", bankAccountDTO.getId(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error("Internal server error while updating bank account with ID: {}", bankAccountDTO.getId(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")     //METHODE - DELETE BANKACCOUNT//
    public ResponseEntity<Object> deleteBankAccount(@PathVariable Long id) {
        logger.info("Request received to delete bank account with ID: {}", id);
        Optional<BankAccount> bankAccount = bankAccountRepository.findById(id);

        if (bankAccount.isPresent()) {
            bankAccountRepository.deleteById(id);
            logger.info("Bank account with ID: {} successfully deleted", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            logger.warn("Bank account with ID: {} not found", id);
            return new ResponseEntity<>("Bank account not found", HttpStatus.NOT_FOUND);
        }
    }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
