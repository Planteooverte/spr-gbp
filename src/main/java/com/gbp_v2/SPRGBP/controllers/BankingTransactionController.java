package com.gbp_v2.SPRGBP.controllers;

import com.gbp_v2.SPRGBP.entities.dto.BankingTransactionDTO;
import com.gbp_v2.SPRGBP.entities.dto.PartialBankingTransactionDTO;
import com.gbp_v2.SPRGBP.entities.model.*;
import com.gbp_v2.SPRGBP.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import com.gbp_v2.SPRGBP.entities.mapper.BankingTransactionMapper;

@RestController
@RequestMapping(path = "/bankingtransaction")

public class BankingTransactionController {

    private static final Logger logger = LoggerFactory.getLogger(BankingTransactionController.class);
    @Autowired
    private BankingTransactionRepository bankingTransactionRepository;

    @Autowired
    private CsvFileRepository csvFileRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping()               //METHODE - READ BANKING TRANSACTIONS//
    public ResponseEntity<?> getBankTransactionByBankAccountId(@RequestParam Long id) {
        List<BankingTransaction> bankingTransactions = bankingTransactionRepository.findByBankAccountId(id);
        if (bankingTransactions == null || bankingTransactions.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No banking transactions found for bankaccountId: " + id);
        }

        List<BankingTransactionDTO> bankingTransactionDTOs = bankingTransactions.stream()
                .map(BankingTransactionMapper::toDTO)
                .collect(Collectors.toList());

        return ResponseEntity.ok(bankingTransactionDTOs);
    }

    @PostMapping()              //METHODE - CREATE BANKING TRANSACTIONS//
    public ResponseEntity<?> addBankingTransactions(@RequestBody List<BankingTransaction> bankingTransactions) {
        try {
            // Étape 1: Récupérer l'email de l'utilisateur authentifié à partir du principal
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = (String) authentication.getPrincipal();

            // Étape 2: Trouver le client par email
            List<Customer> customers = customerRepository.findByEmail(username);
            if (customers.isEmpty()) {
                logger.error("No customer found with email: {}", username);
                return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
            }
            Customer customer = customers.get(0);

            // Liste pour stocker les transactions sauvegardées
            List<BankingTransactionDTO> savedTransactions = new ArrayList<>();

            // Parcourir la liste des transactions pour les vérifier et les sauvegarder
            for (BankingTransaction bankingTransaction : bankingTransactions) {
                Long bankAccountId = bankingTransaction.getBankAccount().getId();
                Long csvFileId = bankingTransaction.getCsvFile().getId();
                logger.info("BankAccount ID: {}", bankAccountId);
                logger.info("CsvFile ID: {}", csvFileId);

                // Étape 3: Vérifier que le bankAccountId appartient au client
                BankAccount bankAccount = bankAccountRepository.findById(bankAccountId)
                        .orElseThrow(() -> new RuntimeException("Bank account not found"));

                if (!bankAccount.getCustomer().getId().equals(customer.getId())) {
                    logger.error("Unauthorized access: Customer ID {} does not own BankAccount ID {}", customer.getId(), bankAccountId);
                    return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
                }

                // Étape 4: Vérifier que le csvFileId existe pour le bankAccountId
                CsvFile csvFile = csvFileRepository.findById(csvFileId)
                        .orElseThrow(() -> new RuntimeException("CsvFile not found"));

                if (!csvFile.getBankAccount().getId().equals(bankAccountId)) {
                    logger.error("CsvFile ID {} does not belong to BankAccount ID {}", csvFileId, bankAccountId);
                    return new ResponseEntity<>("CsvFile does not belong to the bank account", HttpStatus.BAD_REQUEST);
                }

                // Étape 5: Associer les catégories par défaut
                if (bankingTransaction.getDebit() != null && bankingTransaction.getDebit().compareTo(BigDecimal.ZERO) > 0) {
                    Category debitCategory = categoryRepository.findById(16L)
                            .orElseThrow(() -> new RuntimeException("Debit category not found"));
                    bankingTransaction.setCategory(debitCategory);
                } else if (bankingTransaction.getCredit() != null && bankingTransaction.getCredit().compareTo(BigDecimal.ZERO) > 0) {
                    Category creditCategory = categoryRepository.findById(24L)
                            .orElseThrow(() -> new RuntimeException("Credit category not found"));
                    bankingTransaction.setCategory(creditCategory);
                }

                // Étape 6: Sauvegarder l'objet bankingTransaction
                BankingTransaction savedTransaction = bankingTransactionRepository.save(bankingTransaction);

            }

            // Retourner la liste des transactions sauvegardées
            return new ResponseEntity<>("transactions saved", HttpStatus.CREATED);
        } catch (RuntimeException e) {
            logger.error("Runtime exception: {}", e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping()               //METHODE - UPDATE CATEGORY ON BANKING TRANSACTIONS//
    public ResponseEntity<?> updateBankingTransactionOnCategoryId(@RequestBody List<PartialBankingTransactionDTO> requestDTOs) {
        try {
            // Étape 1: Récupérer l'email de l'utilisateur authentifié à partir du principal
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String userEmail = authentication.getName();

            // Étape 2: Trouver le client par email
            List<Customer>  customers = customerRepository.findByEmail(userEmail);
            if (customers == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer not found");
            }

            Customer customer = customers.get(0); // Récupérer le premier customer (un seul dans la liste)

            // Étape 3: Parcourir la liste fournie en entrée et réaliser les actions suivantes
            for (PartialBankingTransactionDTO dto : requestDTOs) {
                // a/ Vérifier que le bankAccountId appartient au client
                if (!bankAccountRepository.existsByIdAndCustomerId(dto.getBankAccountId(), customer.getId())) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("BankAccountId does not belong to the customer");
                }

                // b/ Vérifier que le csvFileId existe pour le bankAccountId
                if (!csvFileRepository.existsByIdAndBankAccountId(dto.getCsvFileId(), dto.getBankAccountId())) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("CsvFileId does not exist for the provided BankAccountId");
                }

                // c/ Associer la catégorie fournie dans l'objet en entrée
                BankingTransaction bankingTransaction = bankingTransactionRepository.findById(dto.getId()).orElse(null);
                if (bankingTransaction == null) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("BankingTransaction not found for ID: " + dto.getId());
                }

                bankingTransaction.setCategory(categoryRepository.findById(dto.getCategoryId()).orElse(null));

                // d/ Sauvegarder en base de données
                bankingTransactionRepository.save(bankingTransaction);
            }

            // Étape 4: Retourner un message "categories updated" + un status HTTP de succès approprié
            return ResponseEntity.ok("Categories updated");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating categories: " + e.getMessage());
        }
    }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.