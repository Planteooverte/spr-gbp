package com.gbp_v2.SPRGBP.controllers;

import com.gbp_v2.SPRGBP.entities.model.Authority;
import com.gbp_v2.SPRGBP.entities.model.Customer;
import com.gbp_v2.SPRGBP.entities.dto.ChangePasswordDataDTO;
import com.gbp_v2.SPRGBP.entities.dto.CustomerDTO;
import com.gbp_v2.SPRGBP.entities.dto.RegistrationDataDTO;
import com.gbp_v2.SPRGBP.mailServices.MailService;
import com.gbp_v2.SPRGBP.securityServices.SecretCodeService;
import com.gbp_v2.SPRGBP.repositories.AuthorityRepository;
import com.gbp_v2.SPRGBP.repositories.CustomerRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.List;


@RestController
public class CustomerController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private SecretCodeService secretCodeService;
    @Autowired
    private MailService mailService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthorityRepository authorityRepository;

    @PostMapping("/register")           //METHOD - FAST REGISTER A USER - No used//
    public ResponseEntity<String> registerUser(@RequestBody Customer customer) {
        //Déclaration et initialisation de variable
        Customer savedCustomer = null;
        ResponseEntity response = null;
        try {
            //Encryption du mot de passe de l'utilisateur
            String hashPwd = passwordEncoder.encode(customer.getPassword());
            customer.setPassword(hashPwd);
            customer.setCreationDate(new Date(System.currentTimeMillis()));
            savedCustomer = customerRepository.save(customer);
            //Vérification si l'utilisateur a été créée en base
            if (savedCustomer.getId() > 0) {

                // Ajouter un droit utilisateur en BDD sur ROLE_USER pour l'id de l'utilisateur
                //SQL request : INSERT INTO public.authorities(customer_id, name_group)	VALUES ('1', 'ROLE_USER');
                Authority authority = new Authority();
                authority.setName("ROLE_USER");
                authority.setCustomer(savedCustomer);
                authorityRepository.save(authority);
                response = ResponseEntity.status(HttpStatus.CREATED).body("Given user details are successfully registered");
            }
        } catch (Exception ex) {
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An exception occured due to " + ex.getMessage());
        }
        return response;
    }

    @GetMapping ("/customer")           //METHOD TO GET CUSTOMER PROFIL AFTER CONNECTION
    public ResponseEntity<Object> getCustomer(@RequestParam String email) {

        // Récupérer l'utilisateur authentifié
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Vérifier si l'utilisateur est authentifié
        if (authentication != null && authentication.isAuthenticated() && !(authentication instanceof AnonymousAuthenticationToken)) {

            // Vérifier que l'email correspond à l'utilisateur authentifié
            if (!authentication.getName().equals(email)) {
                return new ResponseEntity<>("Unauthorized access", HttpStatus.UNAUTHORIZED);
            }

            // Récupérer l'utilisateur par email
            List<Customer> customerList = customerRepository.findByEmail(email);
            if (customerList.isEmpty()) {
                return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
            } else {
                Customer customer = customerList.get(0);

                // Créer un DTO pour renvoyer les données nécessaires
                CustomerDTO customerDTO = new CustomerDTO(
                        customer.getId(),
                        customer.getFirstName(),
                        customer.getLastName(),
                        customer.getEmail(),
                        customer.getRole()
                );

                return new ResponseEntity<>(customerDTO, HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>("Unauthorized access", HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/secretcode")          //METHOD - REGISTRATION PART1 or FORGOTTEN PASSWORD PART1: SEND SECRET CODE TO USER EMAIL
    public ResponseEntity<String> getSecretCode(@RequestParam String email, @RequestParam String typeProcess) {
        logger.info("Received request to generate secret code for email: {} and process: {}", email, typeProcess);

        // Email control
        List<Customer> customers = customerRepository.findByEmail(email);

        if ("registration".equalsIgnoreCase(typeProcess)) {
            if (!customers.isEmpty()) {
                logger.warn("Account already exists for email: {}", email);
                return new ResponseEntity<>("Un compte utilisateur existe déjà pour cet email", HttpStatus.CONFLICT);
            }
        } else if ("forgottenPassword".equalsIgnoreCase(typeProcess)) {
            if (customers.isEmpty()) {
                logger.warn("No account found for email: {}", email);
                return new ResponseEntity<>("Compte utilisateur inconnu", HttpStatus.NOT_FOUND);
            }
        } else if ("changeEmail".equalsIgnoreCase(typeProcess)) {
            if (customers.isEmpty()) {
                logger.info("No account found for email: {}", email);
                // Proceed to generate secret code for new email
            } else {
                logger.warn("Account already exists for email: {}", email);
                return new ResponseEntity<>("Un compte utilisateur existe déjà pour cet email", HttpStatus.CONFLICT);
            }
        } else {
            logger.warn("Unrecognized process type: {}", typeProcess);
            return new ResponseEntity<>("Type de process non reconnu", HttpStatus.BAD_REQUEST);
        }

        // Generate the secret code
        String secretCode = secretCodeService.generateSecretCode();
        logger.info("Generated secret code: {}", secretCode);

        secretCodeService.saveSecretCode(email, secretCode, typeProcess);
        logger.info("Secret code saved in cache for email: {}", email);

        // Upload image
        byte[] imageBytes = null;
        try {
            Path imagePath = (Path) Paths.get("src/main/resources/static/images/GBP_empty_logo.jpg");
            imageBytes = Files.readAllBytes(imagePath);
        } catch (IOException e) {
            logger.error("Error reading image file: {}", e.getMessage());
            return new ResponseEntity<>("Erreur lors de la lecture de l'image", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Adapt the subject of the email according to type_process
        String subject;
        if ("registration".equalsIgnoreCase(typeProcess)) {
            subject = "Inscription - GBP";
        } else if ("forgottenPassword".equalsIgnoreCase(typeProcess)) {
            subject = "Mot de passe oublié - GBP";
        } else if ("changeEmail".equalsIgnoreCase(typeProcess)) {
            subject = "Changement d'email - GBP";
        }else {
            subject = "Process non reconnu - GBP";
        }

        // Building email content with CID
        String content =    "<html>" +
                "<body style='font-family: Arial, sans-serif;'>" +
                "<div style='display: flex; justify-content: center; align-items: center; height: 300px;'>" +
                "<div>" +
                "<p>Bonjour,</p>" +
                "<p>Vous trouverez ci-dessous votre code secret.</p>" +
                "<div style='display: inline-block; padding: 10px; border: 2px solid black;'>" +
                "<strong style='font-size:18px; text-align: center;'>" + secretCode + "</strong>" +
                "</div>" +
                "<p>Ce code sera valide pendant 2 minutes, au-delà de ce délai, vous devrez refaire une demande.</p>" +
                "<p>Cordialement,</p>" +
                "<p>L'équipe GBP</p>" +
                "</div>" +
                "</div>" +
                "<div style='text-align: center; margin-top: 20px;'>" +
                "<img src='cid:GBPLogo' alt='GBP logo' style='max-width: 100%; height: auto;' />" +
                "</div>" +
                "</body>" +
                "</html>";

        // Send the secret code by email with the CID image
        try {
            mailService.sendEmail(email, subject, content, imageBytes, "GBP_empty_logo.jpg");
            logger.info("Secret code email sent successfully to: {} with process: {}", email, typeProcess);
            return new ResponseEntity<>("Le code secret a été envoyé par email", HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error sending email to: {}. Error: {}", email, e.getMessage());
            return new ResponseEntity<>("Erreur lors de l'envoi du code secret : " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/registerNewUser")    //METHOD - REGISTRATION PART2: REGISTER NEW USER
    public ResponseEntity<String> addRegistrationWithSecretCode(@RequestBody RegistrationDataDTO registrationDataDTO) {
        String email = registrationDataDTO.getEmail();
        String providedSecretCode = registrationDataDTO.getSecretCode();
        String typeProcessus = registrationDataDTO.getTypeProcess();

        // 1. Email uniqueness control
        List<Customer> existingCustomers = customerRepository.findByEmail(email);
        if (!existingCustomers.isEmpty()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Email déjà utilisé.");
        }

        // 2. Secret code control
        String correctSecretCode = secretCodeService.getSecretCode(email, typeProcessus);
        if (correctSecretCode == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Code expiré ou incorrect.");
        }

        if (!correctSecretCode.equals(providedSecretCode)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Code incorrect.");
        }

        // 3. Saving the user
        Customer newCustomer = new Customer();
        newCustomer.setFirstName(registrationDataDTO.getFirstName());
        newCustomer.setLastName(registrationDataDTO.getLastName());
        newCustomer.setEmail(email);
        newCustomer.setRole("user");

        // Password encryption
        String encryptedPassword = passwordEncoder.encode(registrationDataDTO.getPassword());
        newCustomer.setPassword(encryptedPassword);

        // Save in Database
        customerRepository.save(newCustomer);

        //4. Retrieve the current date and time in the required format and prepare the variables
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        String formattedDate = now.format(dateFormatter);
        String formattedTime = now.format(timeFormatter);

        // 5. Sending a personalised e-mail
        byte[] imageBytes = null;
        try {
            Path imagePath = (Path) Paths.get("src/main/resources/static/images/GBP_empty_logo.jpg");
            imageBytes = Files.readAllBytes(imagePath);
        } catch (IOException e) {
            logger.error("Error reading image file: {}", e.getMessage());
            return new ResponseEntity<>("Erreur lors de la lecture de l'image", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String subject = "Confirmation inscription GBP";
        String content = String.format(
                "<html>" +
                        "<body style='font-family: Arial, sans-serif;'>" +
                        "<div style='display: flex; justify-content: center; align-items: center; height: 300px;'>" +
                        "<div>" +
                        "<p>Bonjour,</p>" +
                        "<p>Le compte <strong>%s</strong> a été créé le <strong>%s</strong> à <strong>%s</strong>.</p>" +
                        "<p>Vous pouvez accéder à GBP et commencer à bénéficier de nos services.</p>" +
                        "<p>Cordialement,</p>" +
                        "<p>L'équipe GBP</p>" +
                        "</div>" +
                        "</div>" +
                        "<div style='text-align: center; margin-top: 20px;'>" +
                        "<img src='cid:GBPLogo' alt='GBP logo' style='max-width: 100%%; height: auto;' />" +
                        "</div>" +
                        "</body>" +
                        "</html>",
                email, formattedDate, formattedTime
        );
        try {
            mailService.sendEmail(email, subject, content, imageBytes, "GBP_empty_logo.jpg");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de l'envoi de l'email.");
        }

        // 5. HTTP response
        return ResponseEntity.ok("Inscription effectuée avec succès.");
    }

    @PutMapping("/forgottenPassword")   //METHOD - FORGOTTEN PASSWORD PART2: UPDATE PWD
    public ResponseEntity<String> updatePwdWithSecretCode(@RequestBody ChangePasswordDataDTO changePasswordDataDTO) {
        String email = changePasswordDataDTO.getEmail();
        String providedSecretCode = changePasswordDataDTO.getSecretCode();
        String typeProcess = changePasswordDataDTO.getTypeProcess();

        // 1. Check that the email exists
        List<Customer> existingCustomers = customerRepository.findByEmail(email);
        if (existingCustomers.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Compte utilisateur inconnu.");
        }

        // 2. Check the secret code
        String correctSecretCode = secretCodeService.getSecretCode(email, typeProcess);
        if (correctSecretCode == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Code expiré ou incorrect.");
        }

        if (!correctSecretCode.equals(providedSecretCode)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Code incorrect.");
        }

        // 3. Save the new password
        Customer customer = existingCustomers.get(0);
        String encryptedPassword = passwordEncoder.encode(changePasswordDataDTO.getPassword());
        customer.setPassword(encryptedPassword);
        customerRepository.save(customer);

        // 4. Prepare and send the email
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        String formattedDate = now.format(dateFormatter);
        String formattedTime = now.format(timeFormatter);

        byte[] imageBytes = null;
        try {
            Path imagePath = Paths.get("src/main/resources/static/images/GBP_empty_logo.jpg");
            imageBytes = Files.readAllBytes(imagePath);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de la lecture de l'image.");
        }

        String subject = "Confirmation modification du mot de passe GBP";
        String content = String.format(
                "<html>" +
                        "<body style='font-family: Arial, sans-serif;'>" +
                        "<div style='display: flex; justify-content: center; align-items: center; height: 300px;'>" +
                        "<div>" +
                        "<p>Bonjour,</p>" +
                        "<p>Nous vous informons que le mot de passe du compte : <strong>%s</strong> a été changé, le <strong>%s</strong> à <strong>%s</strong>.</p>" +
                        "<p>Si vous n'êtes pas à l'origine de cette demande de changement de mot de passe, nous vous invitons à vérifier votre compte et le cas échéant, à contacter nos services.</p>" +
                        "<p>Cordialement,</p>" +
                        "<p>L'équipe GBP,</p>" +
                        "</div>" +
                        "</div>" +
                        "<div style='text-align: center; margin-top: 20px;'>" +
                        "<img src='cid:GBPLogo' alt='GBP logo' style='max-width: 100%%; height: auto;' />" +
                        "</div>" +
                        "</body>" +
                        "</html>",
                email, formattedDate, formattedTime
        );
        try {
            mailService.sendEmail(email, subject, content, imageBytes, "GBP_empty_logo.jpg");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de l'envoi de l'email.");
        }

        // 5. HTTP Response
        return ResponseEntity.ok("Modification du mot de passe effectuée.");
    }

    @PutMapping("/updateProfil")        //METHOD - UPDATE PROFIL PART2: TO CHANGE EMAIL ADDRESS
    public ResponseEntity<String> updateProfil(@RequestBody RegistrationDataDTO updateProfilDataDTO) {
        logger.info("Received request to update user profil with updateProfilDataDTO: {}", updateProfilDataDTO);

        // Récupération de l'ID de l'utilisateur connecté
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserEmail = authentication.getName();

        // Récupérer le client par email
        List<Customer> customers = customerRepository.findByEmail(currentUserEmail);
        if (customers.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Utilisateur non trouvé.");
        }
        Customer customerToUpdate = customers.get(0);

        //Preparation variable for update the profile
        String email = updateProfilDataDTO.getEmail();
        String providedSecretCode = updateProfilDataDTO.getSecretCode();
        String typeProcess = updateProfilDataDTO.getTypeProcess();

        //Case - User has changed their email address
        if ("changeEmail".equalsIgnoreCase(typeProcess)) {
            // 1. Email uniqueness control
            List<Customer> existingCustomers = customerRepository.findByEmail(email);
            if (!existingCustomers.isEmpty()) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body("Email déjà utilisé.");
            }

            // 2. Secret code control only if email changed
            String correctSecretCode = secretCodeService.getSecretCode(email, typeProcess);
            if (correctSecretCode == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Code expiré ou incorrect.");
            }

            if (!correctSecretCode.equals(providedSecretCode)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Code incorrect.");
            }
        }

        // Case - User has changed their email address or Use has changer one or several property except email address
        if ("changeEmail".equalsIgnoreCase(typeProcess) || "updateProfil".equalsIgnoreCase(typeProcess)) {
            // 3. Saving the user
            customerToUpdate.setFirstName(updateProfilDataDTO.getFirstName());
            customerToUpdate.setLastName(updateProfilDataDTO.getLastName());
            customerToUpdate.setEmail(updateProfilDataDTO.getEmail());

            // Password encryption
            if (updateProfilDataDTO.getPassword() != null && !updateProfilDataDTO.getPassword().isEmpty()) {
                String encryptedPassword = passwordEncoder.encode(updateProfilDataDTO.getPassword());
                customerToUpdate.setPassword(encryptedPassword);
            }

            // Save in Database
            customerRepository.save(customerToUpdate);

            //4. Retrieve the current date and time in the required format and prepare the variables
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

            String formattedDate = now.format(dateFormatter);
            String formattedTime = now.format(timeFormatter);

            // 5. Sending a personalised e-mail
            byte[] imageBytes = null;
            try {
                Path imagePath = (Path) Paths.get("src/main/resources/static/images/GBP_empty_logo.jpg");
                imageBytes = Files.readAllBytes(imagePath);
            } catch (IOException e) {
                logger.error("Error reading image file: {}", e.getMessage());
                return new ResponseEntity<>("Erreur lors de la lecture de l'image", HttpStatus.INTERNAL_SERVER_ERROR);
            }

            String subject = "Confirmation changement d'email GBP";
            String content = String.format(
                    "<html>" +
                            "<body style='font-family: Arial, sans-serif;'>" +
                            "<div style='display: flex; justify-content: center; align-items: center; height: 300px;'>" +
                            "<div>" +
                            "<p>Bonjour,</p>" +
                            "<p>Le profil utilisateur <strong>%s</strong> a été mis à jour le <strong>%s</strong> à <strong>%s</strong>.</p>" +
                            "<p>Vous pouvez accéder aux services GBP avec ce nouvel email.</p>" +
                            "<p>Cordialement,</p>" +
                            "<p>L'équipe GBP</p>" +
                            "</div>" +
                            "</div>" +
                            "<div style='text-align: center; margin-top: 20px;'>" +
                            "<img src='cid:GBPLogo' alt='GBP logo' style='max-width: 100%%; height: auto;' />" +
                            "</div>" +
                            "</body>" +
                            "</html>",
                    email, formattedDate, formattedTime
            );
            try {
                mailService.sendEmail(email, subject, content, imageBytes, "GBP_empty_logo.jpg");
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de l'envoi de l'email.");
            }
        }

        // 6. HTTP response
        return ResponseEntity.ok("Modification du profil effectuée avec succès.");
    }

    @DeleteMapping("/disconnect")       //METHODE - DISCONNECT A USER - TO DO : COMPLETE WITH JWT TABLE MGT
    public ResponseEntity<String> logout() {

        //JWT logic deactivation//
        //////////////////////////
        //////////////////////////

        return ResponseEntity.ok("User disconnected");
    }

    @DeleteMapping("/deleteUser")       //METHODE - DELETE AN USER ACCOUNT WITH ALL ASSOCIATED DATAs
    public ResponseEntity<String> deleteUser() {

        // 1. Recovering the logged-in user
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        logger.info("Received request to delete user profil: {}", email);

        // 2. Delete user account
        List<Customer> customers = customerRepository.findByEmail(email);

        if (customers.isEmpty()) {
            logger.warn("User account with email: {} not found", email);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Retrieve the first user in the list - nota: unique email
        Customer customer = customers.get(0);
        Long id = customer.getId();
        customerRepository.deleteById(id);
        logger.info("Compte utilisateur avec email: {} supprimé avec succès", email);

        // 3. Retrieve the current date and time in the required format and prepare the variables
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        String formattedDate = now.format(dateFormatter);
        String formattedTime = now.format(timeFormatter);

        // 4. Sending a personalised e-mail
        byte[] imageBytes = null;
        try {
            Path imagePath = (Path) Paths.get("src/main/resources/static/images/GBP_empty_logo.jpg");
            imageBytes = Files.readAllBytes(imagePath);
        } catch (IOException e) {
            logger.error("Error reading image file: {}", e.getMessage());
            return new ResponseEntity<>("Erreur lors de la lecture de l'image", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String subject = "Suppression compte GBP";
        String content = String.format(
                "<html>" +
                        "<body style='font-family: Arial, sans-serif;'>" +
                        "<div style='display: flex; justify-content: center; align-items: center; height: 300px;'>" +
                        "<div>" +
                        "<p>Bonjour,</p>" +
                        "<p>Le profil utilisateur <strong>%s</strong> a été supprimé le <strong>%s</strong> à <strong>%s</strong>.</p>" +
                        "<p>N'hésitez pas à revenir nous voir pour de nouveaux services.</p>" +
                        "<p>Cordialement,</p>" +
                        "<p>L'équipe GBP</p>" +
                        "</div>" +
                        "</div>" +
                        "<div style='text-align: center; margin-top: 20px;'>" +
                        "<img src='cid:GBPLogo' alt='GBP logo' style='max-width: 100%%; height: auto;' />" +
                        "</div>" +
                        "</body>" +
                        "</html>",
                email, formattedDate, formattedTime
        );
        try {
            mailService.sendEmail(email, subject, content, imageBytes, "GBP_empty_logo.jpg");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de l'envoi de l'email.");
        }

        // 5. HTTP response
        return ResponseEntity.ok("Suppression du profil effectuée avec succès.");

    }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
