package com.gbp_v2.SPRGBP.controllers;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.model.BankingTransaction;
import com.gbp_v2.SPRGBP.entities.model.Customer;
import com.gbp_v2.SPRGBP.entities.dto.BalanceTampingStatDTO;
import com.gbp_v2.SPRGBP.entities.dto.NbTransactionStatDTO;
import com.gbp_v2.SPRGBP.entities.dto.PercentageCatStatDTO;
import com.gbp_v2.SPRGBP.entities.dto.StatisticalResponse;
import com.gbp_v2.SPRGBP.jobServices.BalanceTampingService;
import com.gbp_v2.SPRGBP.jobServices.NbTransactionService;
import com.gbp_v2.SPRGBP.jobServices.PercentagePerCategoryService;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import com.gbp_v2.SPRGBP.repositories.BankingTransactionRepository;
import com.gbp_v2.SPRGBP.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping(path = "/statistical")

public class StatisticalController {

    private static final Logger logger = LoggerFactory.getLogger(BankingTransactionController.class);
    @Autowired
    private BankingTransactionRepository bankingTransactionRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BalanceTampingService balanceTampingService;

    @Autowired
    private PercentagePerCategoryService percentagePerCategoryService;

    @Autowired
    private NbTransactionService nbTransactionService;

    @GetMapping()               //METHODE - STATISTICAL SYSTEM//
    public ResponseEntity<?> getStatisticalOnBankAccount(@RequestParam Long id) {
        // Recover the authenticated user's email from the main system
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = (String) authentication.getPrincipal();

        // Find the customer by emaill
        List<Customer> customers = customerRepository.findByEmail(username);
        if (customers.isEmpty()) {
            logger.error("No customer found with email: {}", username);
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }
        Customer customer = customers.get(0);

        // Check whether the bank account belongs to the authenticated customer
        BankAccount bankAccount = bankAccountRepository.findById(id).orElse(null);
        if (bankAccount == null || !bankAccount.getCustomer().equals(customer)) {
            logger.error("Unauthorized access to bank account: {}", id);
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }

        // Chek existence of banking transactions with bankAccountId
        List<BankingTransaction> bankingTransactions = bankingTransactionRepository.findByBankAccountId(id);
        if (bankingTransactions == null || bankingTransactions.isEmpty()) {
            logger.error("No transactions found for bank account: {}", id);
            return new ResponseEntity<>("No transactions found for this bank account", HttpStatus.NOT_FOUND);
        }

        // Retrieving statistics from the database
        List<Object[]> totalDebCreStatResults = bankingTransactionRepository.getTotalDebitCreditStatPerMonthAndYear(id);
        if (totalDebCreStatResults == null || totalDebCreStatResults.isEmpty()) {
            logger.error("No debit/credit statistics found for bank account: {}", id);
            return new ResponseEntity<>("No debit/credit statistics found for this bank account", HttpStatus.NOT_FOUND);
        }
        totalDebCreStatResults.forEach(result -> logger.debug("Total Debit Credit Statistical Result: {}", Arrays.toString(result)));

        // Call BalanceTampingService to generate statistics: balanceTampingStatDTOs
        List<BalanceTampingStatDTO> balanceTampingStatDTOs = balanceTampingService.generateBalanceTampingStats(id, totalDebCreStatResults);

        // Retrieving statistics from the database
        List<Object[]> totalCatDebCreStatResults = bankingTransactionRepository.getTotalDebitCreditStatPerCategoryAndMonthAndYear(id);
        if (totalCatDebCreStatResults == null || totalCatDebCreStatResults.isEmpty()) {
            logger.error("No category debit/credit statistics found for bank account: {}", id);
            return new ResponseEntity<>("No category debit/credit statistics found for this bank account", HttpStatus.NOT_FOUND);
        }
        totalCatDebCreStatResults.forEach(result -> logger.debug("Total per Category Debit Credit Statistical Result: {}", Arrays.toString(result)));

        // Call PercentagePerCategoryService to generate statistics: percentageCatStatDTOs
        List<PercentageCatStatDTO> percentageCatStatDTOS = percentagePerCategoryService.calculatePercentagePerCategory(id, totalCatDebCreStatResults, totalDebCreStatResults);

        // Retrieving statistics from the database
        List<Object[]> totalNbTransactionStatResults = bankingTransactionRepository.getTransactionCountPerMonthAndYear(id);
        if (totalNbTransactionStatResults == null || totalNbTransactionStatResults.isEmpty()) {
            logger.error("No transaction count statistics found for bank account: {}", id);
            return new ResponseEntity<>("No transaction count statistics found for this bank account", HttpStatus.NOT_FOUND);
        }
        totalNbTransactionStatResults.forEach(result -> logger.debug("Total transaction per month and year Result: {}", Arrays.toString(result)));

        // Call NbTransactionService to generate statistics: nbTransactionStatDTOs
        List<NbTransactionStatDTO> nbTransactionStatDTOs = nbTransactionService.generateStatutOfBankingTransactions(id, totalNbTransactionStatResults);

        // Create the statistical response
        StatisticalResponse response = new StatisticalResponse(balanceTampingStatDTOs, percentageCatStatDTOS, nbTransactionStatDTOs);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
