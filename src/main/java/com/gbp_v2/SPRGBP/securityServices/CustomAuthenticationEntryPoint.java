package com.gbp_v2.SPRGBP.securityServices;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        if (authException.getMessage().contains("No user registered with this details!")) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Compte utilisateur inconnu.");
        } else if (authException.getMessage().contains("Invalid password")) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Mot de passe incorrect.");
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "Accès non autorisé.");
        }
    }
}
