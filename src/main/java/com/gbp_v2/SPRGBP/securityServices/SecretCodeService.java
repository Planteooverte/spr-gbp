package com.gbp_v2.SPRGBP.securityServices;

import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.bp.LocalDateTime;

@Service
public class SecretCodeService {

    private static final Logger logger = LoggerFactory.getLogger(SecretCodeService.class);
    private static final int CODE_VALIDITY_MINUTES = 2;
    private ConcurrentHashMap<String, CodeDetails> cache = new ConcurrentHashMap<>();

    //Classe interne pour stocker le code secret et son expiration
    private static class CodeDetails {
        String code;
        LocalDateTime expirationTime;
        String typeProcessus;

        public CodeDetails(String code, LocalDateTime expirationTime, String typeProcessus) {
            this.code = code;
            this.expirationTime = expirationTime;
            this.typeProcessus = typeProcessus;
        }

        public String getCode() {
            return code;
        }

        public LocalDateTime getExpirationTime() {
            return expirationTime;
        }

        public String getTypeProcessus() {
            return typeProcessus;
        }
    }

    // Méthode pour générer un code secret
    public String generateSecretCode() {
        Random random = new Random();
        int code = 100000 + random.nextInt(900000);
        logger.debug("Generated secret code: {}", code);
        return String.valueOf(code);
    }

    // Méthode pour sauvegarder le code secret dans le cache avec l'email associé
    public void saveSecretCode(String email, String secretCode, String typeProcessus) {
        LocalDateTime expirationTime = LocalDateTime.now().plusMinutes(CODE_VALIDITY_MINUTES);
        cache.put(email + "_" + typeProcessus, new CodeDetails(secretCode, expirationTime, typeProcessus));
        logger.debug("Saved secret code for email: {} and process: {} with expiration time: {}", email, typeProcessus, expirationTime);
    }

    public String getSecretCode(String email, String typeProcessus) {
        CodeDetails codeDetails = cache.get(email + "_" + typeProcessus);
        if (codeDetails == null) {
            logger.debug("No secret code found for email: {} and process: {}", email, typeProcessus);
            return null;
        }

        if (LocalDateTime.now().isAfter(codeDetails.getExpirationTime())) {
            logger.debug("Secret code for email: {} and process: {} has expired.", email, typeProcessus);
            cache.remove(email + "_" + typeProcessus); // Supprimer le code expiré
            return null;
        }

        logger.debug("Retrieved secret code for email: {} and process: {}: {}", email, typeProcessus, codeDetails.getCode());
        return codeDetails.getCode();
    }
}
