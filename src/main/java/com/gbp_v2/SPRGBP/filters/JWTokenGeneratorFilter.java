package com.gbp_v2.SPRGBP.filters;

import com.gbp_v2.SPRGBP.config.JWTConfig;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class JWTokenGeneratorFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        //1- Si l'objet authentication existe
        if (null != authentication) {
            //Création de la clef de secrete
            SecretKey key = Keys.hmacShaKeyFor(JWTConfig.JWT_KEY.getBytes(StandardCharsets.UTF_8));

            //Création du JWT avec les droits d'accès associé à l'utilisateur
            String jwt = Jwts.builder().setIssuer("GBT").setSubject("JWT Token")
                    .claim("username", authentication.getName())
                    .claim("authorities", populateAuthorities(authentication.getAuthorities()))
                    .setIssuedAt(new Date())
                    .setExpiration(new Date((new Date()).getTime() + JWTConfig.JWT_EXPIRATION))
                    .signWith(key).compact();

            //Renvoie du JWT dans le header de la requête HTTP
            response.setHeader(JWTConfig.JWT_HEADER, jwt);
        }

        filterChain.doFilter(request, response);
    }

    //Méthode pour inclure les droits utilisateur dans le JWT
    private String populateAuthorities(Collection<? extends GrantedAuthority> collection) {
        Set<String> authoritiesSet = new HashSet<>();
        for (GrantedAuthority authority : collection) {
            authoritiesSet.add(authority.getAuthority());
        }
        return String.join(",", authoritiesSet);
    }

    //Méthode pour désactiver la génération du JWT pour la connexion de l'utilisateur
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return !request.getServletPath().equals("/customer");
    }
}
