package com.gbp_v2.SPRGBP.filters;

import com.gbp_v2.SPRGBP.config.JWTConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JWTokenValidatorFilter extends OncePerRequestFilter {
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        //Récupération du JWT contenu dans le header
        String jwt = request.getHeader(JWTConfig.JWT_HEADER);
        if (null != jwt) {
            try {
                //Création de la clef de secrete
                SecretKey key = Keys.hmacShaKeyFor(
                        JWTConfig.JWT_KEY.getBytes(StandardCharsets.UTF_8));

                //Vérification du JWT donné avec la clef secret et récupérations des revendications
                Claims claims = Jwts.parserBuilder()
                        .setSigningKey(key)
                        .build()
                        .parseClaimsJws(jwt)
                        .getBody();

                //Récupération des informations d'identification de l'utilisateur sans pwd
                String username = String.valueOf(claims.get("username"));
                String authorities = (String) claims.get("authorities");

                //Génération d'un token d'authentification qu'on sauvegarde dans le context security
                Authentication auth = new UsernamePasswordAuthenticationToken(username, null,
                        AuthorityUtils.commaSeparatedStringToAuthorityList(authorities));
                SecurityContextHolder.getContext().setAuthentication(auth);
            } catch (Exception e) {
                throw new BadCredentialsException("Invalid Token received!");
            }

        }
        filterChain.doFilter(request, response);
    }

    //Méthode pour désactiver la validation du JWT pour la connexion de l'utilisation
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return request.getServletPath().equals("/customer");
    }

}
