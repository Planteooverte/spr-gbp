package com.gbp_v2.SPRGBP.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@Component
public class CsrfTokenFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(CsrfTokenFilter.class);

    private final CsrfTokenRepository csrfTokenRepository;

    public CsrfTokenFilter() {
        // Utilisation d'un HttpSessionCsrfTokenRepository pour gérer le token dans la session.
        this.csrfTokenRepository = new HttpSessionCsrfTokenRepository();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        /// Obtenir ou générer le token CSRF
        CsrfToken csrfToken = csrfTokenRepository.loadToken(request);

        if (csrfToken == null) {
            logger.info("Aucun token CSRF trouvé. Génération d'un nouveau token CSRF.");
            csrfToken = csrfTokenRepository.generateToken(request);
            csrfTokenRepository.saveToken(csrfToken, request, response);
        } else {
            logger.info("Token CSRF existant chargé depuis la requête: {}", csrfToken.getToken());
        }

        // Ajouter le token au header de la réponse pour les clients
        response.setHeader("X-CSRF-TOKEN", csrfToken.getToken());
        response.setHeader("Access-Control-Expose-Headers", "Authorization, x-csrf-token");

        // Log du token envoyé au client
        logger.info("Token CSRF envoyé dans la réponse: {}", csrfToken.getToken());

        // Continuer le filtre suivant
        filterChain.doFilter(request, response);
    }
}

