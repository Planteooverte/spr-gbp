package com.gbp_v2.SPRGBP.entities.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class BankingTransactionDTO {
    private Long id;
    private LocalDate date;
    private String description;
    private BigDecimal credit;
    private BigDecimal debit;
    private Date creationDate;
    private Date updateDate;
    private Long bankAccountId;
    private Long categoryId;
    private Long csvFileId;
    private String fileName;
    private String categoryName;
    private String transactionType;

    public BankingTransactionDTO(Long id, LocalDate date, String description, BigDecimal credit, BigDecimal debit,
                                 Date creationDate, Date updateDate, Long bankAccountId, Long categoryId,
                                 Long csvFileId, String fileName, String categoryName, String transactionType) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.credit = credit;
        this.debit = debit;
        this.creationDate = creationDate;
        this.updateDate = updateDate;
        this.bankAccountId = bankAccountId;
        this.categoryId = categoryId;
        this.csvFileId = csvFileId;
        this.fileName = fileName;
        this.categoryName = categoryName;
        this.transactionType = transactionType;
    }
}
