package com.gbp_v2.SPRGBP.entities.dto;

public class NbTransactionStatDTO {
    private Integer year;
    private Integer month;
    private Boolean attendanceRecord;
    private Integer nbTransactions;

    // Getters and Setters
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Boolean getAttendanceRecord() {
        return attendanceRecord;
    }

    public void setAttendanceRecord(Boolean attendanceRecord) {
        this.attendanceRecord = attendanceRecord;
    }

    public Integer getNbTransactions() {
        return nbTransactions;
    }

    public void setNbTransactions(Integer nbTransactions) {
        this.nbTransactions = nbTransactions;
    }
}
