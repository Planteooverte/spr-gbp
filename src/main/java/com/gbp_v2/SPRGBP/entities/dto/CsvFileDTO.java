package com.gbp_v2.SPRGBP.entities.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class CsvFileDTO {
    private Long id;
    private String fileName;
    private Date creationDate;
    private Date updateDate;
    private Long bankAccountId;

    public CsvFileDTO(Long id, String fileName, Date creationDate, Date updateDate, Long bankAccountId) {
        this.id = id;
        this.fileName = fileName;
        this.creationDate = creationDate;
        this.updateDate = updateDate;
        this.bankAccountId = bankAccountId;
    }
}

