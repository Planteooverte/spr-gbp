package com.gbp_v2.SPRGBP.entities.dto;

import java.util.List;

public class PartialBankingTransactionDTO {
    private Long id;
    private Long csvFileId;
    private Long categoryId;
    private Long bankAccountId;

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCsvFileId() {
        return csvFileId;
    }

    public void setCsvFileId(Long csvFileId) {
        this.csvFileId = csvFileId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

}
