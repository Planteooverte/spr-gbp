package com.gbp_v2.SPRGBP.entities.dto;
import com.gbp_v2.SPRGBP.entities.list.TypeAccount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LongCsvFileDTO {
    private Long id;
    private String fileName;
    private Date creationDate;
    private Date updateDate;
    private Long bankAccountId;
    private String bankName;
    private String referenceAccount;
    private TypeAccount typeAccount;
}
