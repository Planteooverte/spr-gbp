package com.gbp_v2.SPRGBP.entities.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gbp_v2.SPRGBP.entities.list.TypeAccount;
import lombok.Data;

import java.math.BigDecimal;

@Data

public class BankAccountDTO {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("referenceAccount")
    private String referenceAccount;

    @JsonProperty("bankName")
    private String bankName;

    @JsonProperty("address")
    private String address;

    @JsonProperty("city")
    private String city;

    @JsonProperty("typeAccount")
    private String typeAccount;

    @JsonProperty("initialBalance")
    private BigDecimal initialBalance;

    @JsonProperty("customerId")
    private Long customerId;

    // Constructor
    public BankAccountDTO(
            Long id,
            String referenceAccount,
            String bankName,
            String address,
            String city,
            TypeAccount typeAccount,
            BigDecimal initialBalance,
            Long customerId) {
        this.id = id;
        this.referenceAccount = referenceAccount;
        this.bankName = bankName;
        this.address = address;
        this.city = city;
        this.typeAccount = String.valueOf(typeAccount);
        this.initialBalance = initialBalance;
        this.customerId = customerId;
    }
}