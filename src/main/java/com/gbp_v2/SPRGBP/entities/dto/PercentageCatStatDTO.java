package com.gbp_v2.SPRGBP.entities.dto;

import java.util.Objects;

public class PercentageCatStatDTO {
    private Integer year;
    private Integer month;
    private String categoryName;
    private Double totalCatCredit;
    private Double totalCatDebit;
    private Double percentTotalCatCredit;
    private Double percentTotalCatDebit;

    // Getters et Setters
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getTotalCatCredit() {
        return totalCatCredit;
    }

    public void setTotalCatCredit(Double totalCatCredit) {
        this.totalCatCredit = totalCatCredit;
    }

    public Double getTotalCatDebit() {
        return totalCatDebit;
    }

    public void setTotalCatDebit(Double totalCatDebit) {
        this.totalCatDebit = totalCatDebit;
    }

    public Double getPercentTotalCatCredit() {
        return percentTotalCatCredit;
    }

    public void setPercentTotalCatCredit(Double percentTotalCatCredit) {
        this.percentTotalCatCredit = percentTotalCatCredit;
    }

    public Double getPercentTotalCatDebit() {
        return percentTotalCatDebit;
    }

    public void setPercentTotalCatDebit(Double percentTotalCatDebit) {
        this.percentTotalCatDebit = percentTotalCatDebit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PercentageCatStatDTO that = (PercentageCatStatDTO) o;
        return year == that.year &&
                month == that.month &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(totalCatCredit, that.totalCatCredit) &&
                Objects.equals(totalCatDebit, that.totalCatDebit) &&
                Objects.equals(percentTotalCatCredit, that.percentTotalCatCredit) &&
                Objects.equals(percentTotalCatDebit, that.percentTotalCatDebit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, categoryName, totalCatCredit, totalCatDebit, percentTotalCatCredit, percentTotalCatDebit);
    }
}
