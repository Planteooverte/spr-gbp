package com.gbp_v2.SPRGBP.entities.dto;

import java.util.List;

public class StatisticalResponse {
    private List<BalanceTampingStatDTO> balanceTampingStatDTOs;
    private List<PercentageCatStatDTO> percentageCatStatDTOS;
    private List<NbTransactionStatDTO> nbTransactionStatDTOs;

    public StatisticalResponse(List<BalanceTampingStatDTO> balanceTampingStatDTOs, List<PercentageCatStatDTO> percentageCatStatDTOS, List<NbTransactionStatDTO> nbTransactionStatDTOs) {
        this.balanceTampingStatDTOs = balanceTampingStatDTOs;
        this.percentageCatStatDTOS = percentageCatStatDTOS;
        this.nbTransactionStatDTOs = nbTransactionStatDTOs;
    }

    // Getters and Setters
    public List<BalanceTampingStatDTO> getBalanceTampingStatDTOs() {
        return balanceTampingStatDTOs;
    }

    public void setBalanceTampingStatDTOs(List<BalanceTampingStatDTO> balanceTampingStatDTOs) {
        this.balanceTampingStatDTOs = balanceTampingStatDTOs;
    }

    public List<PercentageCatStatDTO> getPercentageCatStatDTOs() {
        return percentageCatStatDTOS;
    }

    public void setPercentageCatStatDTOs(List<PercentageCatStatDTO> percentageCatStatDTOS) {
        this.percentageCatStatDTOS = percentageCatStatDTOS;
    }

    public List<NbTransactionStatDTO> getNbTransactionStatDTOs() {
        return nbTransactionStatDTOs;
    }

    public void setNbTransactionStatDTOs(List<NbTransactionStatDTO> nbTransactionStatDTOs) {
        this.nbTransactionStatDTOs = nbTransactionStatDTOs;
    }
}
