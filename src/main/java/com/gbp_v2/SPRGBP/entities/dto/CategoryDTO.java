package com.gbp_v2.SPRGBP.entities.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class CategoryDTO {
    private Long id;
    private String categoryName;
    private String categoryType;

    public CategoryDTO(Long id, String categoryName, String categoryType){
        this.id = id;
        this.categoryName = categoryName;
        this.categoryType = categoryType;
    }
}
