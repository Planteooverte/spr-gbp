package com.gbp_v2.SPRGBP.entities.dto;

import java.util.Objects;

public class BalanceTampingStatDTO {
    private Integer year;
    private Integer month;
    private Double totalDebit;
    private Double totalCredit;
    private Double tampingBalance;

    // Getters and Setters
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(Double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public Double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Double totalCredit) {
        this.totalCredit = totalCredit;
    }

    public Double getTampingBalance() {
        return tampingBalance;
    }

    public void setTampingBalance(Double tampingBalance) {
        this.tampingBalance = tampingBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceTampingStatDTO that = (BalanceTampingStatDTO) o;
        return year == that.year &&
                month == that.month &&
                Double.compare(that.totalDebit, totalDebit) == 0 &&
                Double.compare(that.totalCredit, totalCredit) == 0 &&
                Double.compare(that.tampingBalance, tampingBalance) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, totalDebit, totalCredit, tampingBalance);
    }
}
