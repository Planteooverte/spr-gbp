package com.gbp_v2.SPRGBP.entities.mapper;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.dto.BankAccountDTO;
import com.gbp_v2.SPRGBP.entities.model.Customer;
import com.gbp_v2.SPRGBP.entities.list.TypeAccount;

public class BankAccountMapper {

    public static BankAccountDTO toDTO(BankAccount bankAccount) {
        return new BankAccountDTO(
                bankAccount.getId(),
                bankAccount.getReferenceAccount(),
                bankAccount.getBankName(),
                bankAccount.getAddress(),
                bankAccount.getCity(),
                bankAccount.getTypeAccount(),
                bankAccount.getInitialBalance(),
                bankAccount.getCustomer().getId()
        );
    }

    public static BankAccount toEntity(BankAccountDTO dto, Customer customer) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(dto.getId());
        bankAccount.setReferenceAccount(dto.getReferenceAccount());
        bankAccount.setBankName(dto.getBankName());
        bankAccount.setAddress(dto.getAddress());
        bankAccount.setCity(dto.getCity());
        bankAccount.setTypeAccount(TypeAccount.valueOf(dto.getTypeAccount()));
        bankAccount.setInitialBalance(dto.getInitialBalance());
        bankAccount.setCustomer(customer);
        return bankAccount;
    }
}
