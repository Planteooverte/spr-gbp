package com.gbp_v2.SPRGBP.entities.mapper;

import com.gbp_v2.SPRGBP.entities.model.BankingTransaction;
import com.gbp_v2.SPRGBP.entities.dto.BankingTransactionDTO;
import com.gbp_v2.SPRGBP.entities.dto.PartialBankingTransactionDTO;

import java.util.List;
import java.util.stream.Collectors;

public class BankingTransactionMapper {
    public static BankingTransactionDTO toDTO(BankingTransaction bankingTransaction) {
        if (bankingTransaction == null) {
            return null;
        }

        BankingTransactionDTO dto = new BankingTransactionDTO();
        dto.setId(bankingTransaction.getId());
        dto.setDate(bankingTransaction.getDate());
        dto.setDescription(bankingTransaction.getDescription());
        dto.setCredit(bankingTransaction.getCredit());
        dto.setDebit(bankingTransaction.getDebit());
        dto.setCreationDate(bankingTransaction.getCreationDate());
        dto.setUpdateDate(bankingTransaction.getUpdateDate());
        dto.setBankAccountId(bankingTransaction.getBankAccount().getId());
        dto.setCategoryId(bankingTransaction.getCategory() != null ? bankingTransaction.getCategory().getId() : null);
        dto.setCsvFileId(bankingTransaction.getCsvFile() != null ? bankingTransaction.getCsvFile().getId() : null);
        dto.setFileName(bankingTransaction.getCsvFile() != null ? bankingTransaction.getCsvFile().getFileName() : null);
        dto.setCategoryName(bankingTransaction.getCategory() != null ? bankingTransaction.getCategory().getCategoryName().toString() : null);

        // Définir le type de transaction
        if (bankingTransaction.getCredit() == null && bankingTransaction.getDebit() != null) {
            dto.setTransactionType("debit");
        } else if (bankingTransaction.getDebit() == null && bankingTransaction.getCredit() != null) {
            dto.setTransactionType("credit");
        } else {
            dto.setTransactionType("unknown");
        }

        return dto;
    }

    public static PartialBankingTransactionDTO toPartialDTO(BankingTransaction bankingTransaction) {
        PartialBankingTransactionDTO dto = new PartialBankingTransactionDTO();
        dto.setId(bankingTransaction.getId());
        dto.setCsvFileId(bankingTransaction.getCsvFile().getId());
        dto.setCategoryId(bankingTransaction.getCategory().getId());
        dto.setBankAccountId(bankingTransaction.getBankAccount().getId());
        return dto;
    }

    public static List<PartialBankingTransactionDTO> toPartialDTOs(List<BankingTransaction> bankingTransactions) {
        return bankingTransactions.stream()
                .map(BankingTransactionMapper::toPartialDTO)
                .collect(Collectors.toList());
    }

}
