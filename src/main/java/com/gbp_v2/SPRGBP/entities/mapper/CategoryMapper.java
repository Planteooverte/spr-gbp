package com.gbp_v2.SPRGBP.entities.mapper;

import com.gbp_v2.SPRGBP.entities.model.Category;
import com.gbp_v2.SPRGBP.entities.dto.CategoryDTO;

public class CategoryMapper {

    public static CategoryDTO toDTO(Category category) {
        if (category == null){
            return null;
        }

        CategoryDTO dto = new CategoryDTO();
        dto.setId(category.getId());
        dto.setCategoryName(String.valueOf(category.getCategoryName()));
        dto.setCategoryType(String.valueOf(category.getCategoryType()));

        return dto;
    }
}
