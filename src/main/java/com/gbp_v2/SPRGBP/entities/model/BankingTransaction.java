package com.gbp_v2.SPRGBP.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import java.time.LocalDate;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="bankingTransactions")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class BankingTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private LocalDate date;

    @Column(nullable = false)
    private String description;

    @Column(nullable = true)
    private BigDecimal credit;

    @Column(nullable = true)
    private BigDecimal debit;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Date creationDate;

    @Column(nullable = false)
    @UpdateTimestamp
    private Date updateDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "csvFile_id", nullable = false, foreignKey = @ForeignKey(name = "fk_bankingtransaction_csvfile"))
    private CsvFile csvFile;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bankAccount_id", nullable = false, foreignKey = @ForeignKey(name = "fk_bankingtransaction_bankaccount"))
    private BankAccount bankAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", nullable = false, foreignKey = @ForeignKey(name = "fk_bankingtransaction_category"))
    private Category category;
}