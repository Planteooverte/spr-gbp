package com.gbp_v2.SPRGBP.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.List;


@Entity
@Table(name="csvFiles")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class CsvFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String fileName;

    @Temporal(TemporalType.TIMESTAMP)                       //Mapping de la colonne vers un type de donnée temporel
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Date creationDate;

    @Column(nullable = false)
    @UpdateTimestamp
    private Date updateDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bankAccount_id", nullable = false, foreignKey = @ForeignKey(name = "fk_csvfile_bankaccount"))
    private BankAccount bankAccount;

    @JsonIgnore
    @OneToMany(mappedBy = "csvFile", cascade = CascadeType.ALL)
    private List<BankingTransaction> bankingTransactions;
}