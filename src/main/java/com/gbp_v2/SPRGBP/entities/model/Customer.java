package com.gbp_v2.SPRGBP.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity                       //Entité JPA qui sera mappée à une table en BDD
@Table(name="customers")      //Nom de la table en BDD
@Data                         //Génére automatiquement les méthodes 'toString', 'equals', 'hashCode', 'getters' et 'setters'
@NoArgsConstructor            //Génère un constructeur sans argument
@AllArgsConstructor           //Génère un constructeur avec arguments
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})    //Permet d'éviter les problèmes de boucle infinie
public class Customer {

    @Id                                                     //Clef primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY)     //Auto-incrémentation
    @Column(nullable = false)                               //Détail de la colonne, null ou non null
    private Long id;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String firstName;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(nullable = false)
    private String password;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private  String role;

    @Temporal(TemporalType.TIMESTAMP)                       //Mapping de la colonne vers un type de donnée temporel
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Date creationDate;

    @Column(nullable = false)
    @UpdateTimestamp
    private Date updateDate;

    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade = CascadeType.REMOVE)    //la suppression d'un customer entraine une suppression en cascade
    private List<BankAccount> bankAccounts;

    @JsonIgnore
    @OneToMany(mappedBy = "customer", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)    //la suppression d'un customer entraine une suppression en cascade
    private Set<Authority> authorities;

    public Set<Authority> getAuthorities() {
        return authorities;
    }

}
