package com.gbp_v2.SPRGBP.entities.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gbp_v2.SPRGBP.entities.list.TypeAccount;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="bankAccounts")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})    //Permet d'éviter les problèmes de boucle infinie

public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false, unique = true)
    private String referenceAccount;

    @Column(nullable = false)
    private String bankName;

    @Column(nullable = true)
    private String address;

    @Column(nullable = true)
    private String city;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeAccount typeAccount;

    @Column(nullable = true)
    private BigDecimal initialBalance;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Date creationDate;

    @Column(nullable = false)
    @UpdateTimestamp
    private Date updateDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false, foreignKey = @ForeignKey(name = "fk_bankaccount_customer"))
    private Customer customer;

    @JsonIgnore
    @OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL)
    private List<CsvFile> csvFiles;

    @JsonIgnore
    @OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL)
    private List<BankingTransaction> bankingTransactions;
}
