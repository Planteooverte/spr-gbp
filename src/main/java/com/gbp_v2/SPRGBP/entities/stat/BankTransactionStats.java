package com.gbp_v2.SPRGBP.entities.stat;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class BankTransactionStats {
    private List<Object[]> monthlyBalance;
    private List<Object[]> totalCredit;
    private List<Object[]> totalDebit;
    private List<Object[]> percentageCategoriesByCredit;
    private List<Object[]> percentageCategoriesByDebit;

    public BankTransactionStats(List<Object[]> monthlyBalance, List<Object[]> totalCredit,
                                List<Object[]> totalDebit, List<Object[]> percentageCategoriesByCredit,
                                List<Object[]> percentageCategoriesByDebit) {
        this.monthlyBalance = monthlyBalance;
        this.totalCredit = totalCredit;
        this.totalDebit = totalDebit;
        this.percentageCategoriesByCredit = percentageCategoriesByCredit;
        this.percentageCategoriesByDebit = percentageCategoriesByDebit;
    }

}