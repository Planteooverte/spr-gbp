package com.gbp_v2.SPRGBP.entities.list;

public enum Unit {
    sans,
    m3,
    kW,
    l
}
