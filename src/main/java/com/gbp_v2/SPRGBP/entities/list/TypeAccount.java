package com.gbp_v2.SPRGBP.entities.list;

public enum TypeAccount {
    Assurance_vie("Assurance vie"),
    CAT("Compte à terme"),
    CEL("Compte épargne logement"),
    Compte_courant("Compte courant"),
    Compte_joint("Compte joint"),
    LDDS("Livret de développement durable et solidaire"),
    Livret_A("Livret A"),
    LEP("Livret d'épargne populaire"),
    PEA("Plan d'épargne en action"),
    PEL("Plan d'épargne logement"),
    PEE("Plan d'épargne entreprise"),
    PERP("Plan d'épargne retraite populaire"),
    Non_defini("Non défini");

    private  String label;

    TypeAccount(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
