package com.gbp_v2.SPRGBP.jobServices;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.dto.NbTransactionStatDTO;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class NbTransactionService {

    private static final Logger logger = LoggerFactory.getLogger(NbTransactionService.class);

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public List<NbTransactionStatDTO> generateStatutOfBankingTransactions(Long bankAccountId, List<Object[]> totalNbTransactionStatResults) {

        // Checking existence of bankAccount
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(bankAccountId);
        if (!optionalBankAccount.isPresent()) {
            throw new IllegalArgumentException("Bank account not found");
        }

        // Preparation des variables pour la boucle
        int yearBeg = totalNbTransactionStatResults.stream()
                .map(entry -> ((Number) entry[0]).intValue())
                .min(Integer::compare)
                .orElseThrow(() -> new IllegalArgumentException("No year data available"));
        int monthBeg = 1;
        int yearEnd = totalNbTransactionStatResults.stream()
                .map(entry -> ((Number) entry[0]).intValue())
                .max(Integer::compare)
                .orElseThrow(() -> new IllegalArgumentException("No year data available"));
        int monthEnd = totalNbTransactionStatResults.stream()
                .filter(entry -> ((Number) entry[0]).intValue() == yearEnd)
                .map(entry -> ((Number) entry[1]).intValue())
                .max(Integer::compare)
                .orElseThrow(() -> new IllegalArgumentException("No month data available for the largest year"));
        int month = monthBeg;
        int year = yearBeg;
        List<NbTransactionStatDTO> nbTransactionStatDTOList = new ArrayList<>();

        // Loop - Preparation for object list nbTransactionStatDTO
        while (year < yearEnd || (year == yearEnd && month <= monthEnd)) {
            logger.debug("Boucle year: {}, month: {}", year, month);

            // Variable declaration
            final int currentYear = year;
            final int currentMonth = month;

            // Search and recovery
            Optional<Object[]> matchingEntry = totalNbTransactionStatResults.stream()
                    .filter(result -> ((Number) result[0]).intValue() == currentYear && ((Number) result[1]).intValue() == currentMonth)
                    .findFirst();

            NbTransactionStatDTO nbTransactionStatDTO = new NbTransactionStatDTO();
            nbTransactionStatDTO.setYear(year);
            nbTransactionStatDTO.setMonth(month);

            if (matchingEntry.isPresent()) {
                Object[] entry = matchingEntry.get();
                nbTransactionStatDTO.setAttendanceRecord(true);
                nbTransactionStatDTO.setNbTransactions(((Number) entry[2]).intValue());
            } else {
                nbTransactionStatDTO.setAttendanceRecord(false);
                nbTransactionStatDTO.setNbTransactions(0);
            }

            // Log the DTO
            logger.debug("Generated NbTransactionStatDTOList: {}", nbTransactionStatDTOList);

            nbTransactionStatDTOList.add(nbTransactionStatDTO);

            //counter incrementation
            if (month == 12) {
                year++;
                month = 1;
            } else {
                month++;
            }
        }

        return nbTransactionStatDTOList;
    }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
