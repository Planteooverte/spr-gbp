package com.gbp_v2.SPRGBP.jobServices;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.dto.PercentageCatStatDTO;
import com.gbp_v2.SPRGBP.entities.list.CategoryName;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PercentagePerCategoryService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public List<PercentageCatStatDTO> calculatePercentagePerCategory(Long bankAccountId, List<Object[]> totalCatDebCreStatResults, List<Object[]> totalDebCreStatResults) {
        //Checking existence of bankAccount
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(bankAccountId);
        if (!optionalBankAccount.isPresent()) {
            throw new IllegalArgumentException("Bank account not found");
        }

        //Preparing Loop variables
        int yearBeg = totalDebCreStatResults.stream().mapToInt(entry -> ((Number) entry[0]).intValue()).min().orElseThrow();
        int yearEnd = totalDebCreStatResults.stream().mapToInt(entry -> ((Number) entry[0]).intValue()).max().orElseThrow();
        int monthBeg = 1;
        int monthEnd = totalDebCreStatResults.stream()
                .filter(entry -> ((Number) entry[0]).intValue() == yearEnd)
                .mapToInt(entry -> ((Number) entry[1]).intValue())
                .max()
                .orElse(12);
        int month = monthBeg;
        int year = yearBeg;
        List<PercentageCatStatDTO> percentageCatStatDTOList = new ArrayList<>();

        //Loop - Calculation PercentageCatStatDTO
        while (year < yearEnd || (year == yearEnd && month <= monthEnd)) {

            //Variable declaration
            double totalDebit;
            double totalCredit;
            int finalYear = year;
            int finalMonth = month;

            //Search and recovery
            Optional<Object[]> matchingEntry = totalDebCreStatResults.stream()
                    .filter(entry -> ((Number) entry[0]).intValue() == finalYear && ((Number) entry[1]).intValue() == finalMonth)
                    .findFirst();

            if (matchingEntry.isPresent()) {
                Object[] entry = matchingEntry.get();
                totalDebit = entry[2] != null ? ((Number) entry[2]).doubleValue() : 0;
                totalCredit = entry[3] != null ? ((Number) entry[3]).doubleValue() : 0;
            } else {
                totalDebit = 0;
                totalCredit = 0;
            }

            //Allocation and Saving property in percentageCatStatDTOList object
            if (matchingEntry.isPresent()) {
                totalCatDebCreStatResults.stream()
                        .filter(entry -> ((Number) entry[0]).intValue() == finalYear && ((Number) entry[1]).intValue() == finalMonth)
                        .forEach(entry -> {
                            BigDecimal totalCatCredit = (BigDecimal) entry[3];
                            BigDecimal totalCatDebit = (BigDecimal) entry[4];

                            PercentageCatStatDTO percentageCatStatDTO = new PercentageCatStatDTO();
                            percentageCatStatDTO.setYear(finalYear);
                            percentageCatStatDTO.setMonth(finalMonth);
                            percentageCatStatDTO.setCategoryName(((CategoryName) entry[2]).name());
                            percentageCatStatDTO.setTotalCatCredit(totalCatCredit != null ? totalCatCredit.doubleValue() : null);
                            percentageCatStatDTO.setTotalCatDebit(totalCatDebit != null ? totalCatDebit.doubleValue() : null);
                            percentageCatStatDTO.setPercentTotalCatCredit(totalCredit != 0 && totalCatCredit != null ? (percentageCatStatDTO.getTotalCatCredit() / totalCredit) * 100 : null);
                            percentageCatStatDTO.setPercentTotalCatDebit(totalDebit != 0 && totalCatDebit != null ? (percentageCatStatDTO.getTotalCatDebit() / totalDebit) * 100 : null);

                            percentageCatStatDTOList.add(percentageCatStatDTO);
                        });
            } else {
                PercentageCatStatDTO percentageCatStatDTO = new PercentageCatStatDTO();
                percentageCatStatDTO.setYear(year);
                percentageCatStatDTO.setMonth(month);
                percentageCatStatDTO.setCategoryName("no data");
                percentageCatStatDTO.setTotalCatCredit(null);
                percentageCatStatDTO.setTotalCatDebit(null);
                percentageCatStatDTO.setPercentTotalCatCredit(null);
                percentageCatStatDTO.setPercentTotalCatDebit(null);
                percentageCatStatDTOList.add(percentageCatStatDTO);
            }

            //counter incrementation
            if (month == 12) {
                year++;
                month = 1;
            } else {
                month++;
            }
        }

        return percentageCatStatDTOList;
    }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
