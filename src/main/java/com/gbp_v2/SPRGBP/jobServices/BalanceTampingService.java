package com.gbp_v2.SPRGBP.jobServices;

import com.gbp_v2.SPRGBP.entities.model.BankAccount;
import com.gbp_v2.SPRGBP.entities.dto.BalanceTampingStatDTO;
import com.gbp_v2.SPRGBP.repositories.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class BalanceTampingService {

    private static final Logger logger = LoggerFactory.getLogger(BalanceTampingService.class);

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public List<BalanceTampingStatDTO> generateBalanceTampingStats(Long bankAccountId, List<Object[]> totalDebCreStatResults) {
        //Checking existence of bankAccount
        Optional<BankAccount> optionalBankAccount = bankAccountRepository.findById(bankAccountId);
        if (!optionalBankAccount.isPresent()) {
            throw new IllegalArgumentException("Bank account not found");
        }

        //Preparing Loop variables
        int yearBeg = totalDebCreStatResults.stream()
                .map(entry -> ((Number) entry[0]).intValue())
                .min(Integer::compare)
                .orElseThrow(() -> new IllegalArgumentException("No year data available"));
        int monthBeg = 1;
        int yearEnd = totalDebCreStatResults.stream()
                .map(entry -> ((Number) entry[0]).intValue())
                .max(Integer::compare)
                .orElseThrow(() -> new IllegalArgumentException("No year data available"));
        int monthEnd = totalDebCreStatResults.stream()
                .filter(entry -> ((Number) entry[0]).intValue() == yearEnd)
                .map(entry -> ((Number) entry[1]).intValue())
                .max(Integer::compare)
                .orElseThrow(() -> new IllegalArgumentException("No month data available for the largest year"));
        int month = monthBeg;
        int year = yearBeg;
        BankAccount bankAccount = optionalBankAccount.get();
        double tampingBalance = bankAccount.getInitialBalance().doubleValue();
        List<BalanceTampingStatDTO> balanceTampingStatDTOList = new ArrayList<>();

        //Loop - Calculation balanceTampingStatDTOList
        while (year < yearEnd || (year == yearEnd && month <= monthEnd)) {

            //Variable declaration
            double totalDebit = 0;
            double totalCredit = 0;
            int finalYear = year;
            int finalMonth = month;

            //Search and recovery
            Optional<Object[]> matchingEntry = totalDebCreStatResults.stream()
                    .filter(entry -> ((Number) entry[0]).intValue() == finalYear && ((Number) entry[1]).intValue() == finalMonth)
                    .findFirst();

            if (matchingEntry.isPresent()) {
                Object[] entry = matchingEntry.get();
                totalDebit = entry[2] != null ? ((Number) entry[2]).doubleValue() : 0;
                totalCredit = entry[3] != null ? ((Number) entry[3]).doubleValue() : 0;
            } else {
                totalDebit = 0;
                totalCredit = 0;
            }

            //TampingBalance calculation
            double balance = totalCredit - totalDebit;
            tampingBalance += balance;

            //Allocation and Saving property in balanceTampingStatDTO object
            BalanceTampingStatDTO balanceTampingStatDTO = new BalanceTampingStatDTO();
            balanceTampingStatDTO.setYear(year);
            balanceTampingStatDTO.setMonth(month);
            balanceTampingStatDTO.setTotalDebit(totalDebit);
            balanceTampingStatDTO.setTotalCredit(totalCredit);
            balanceTampingStatDTO.setTampingBalance(tampingBalance);

            // Log the DTO
            logger.debug("Generated BalanceTampingStatDTO: {}", balanceTampingStatDTO);

            balanceTampingStatDTOList.add(balanceTampingStatDTO);

            //counter incrementation
            if (month == 12) {
                year++;
                month = 1;
            } else {
                month++;
            }
        }

        return balanceTampingStatDTOList;
    }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
