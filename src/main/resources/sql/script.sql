-- Contrainte CASCADE ON UPDATE et ON DELETE sur les clefs étrangères --
ALTER TABLE "bank_accounts"
DROP CONSTRAINT IF EXISTS fk_bankaccount_customer,
ADD CONSTRAINT fk_bankaccount_customer
FOREIGN KEY ("customer_id")
REFERENCES "customers"("id")
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE "csv_files"
DROP CONSTRAINT IF EXISTS fk_csvfile_bankaccount,
ADD CONSTRAINT fk_csvfile_bankaccount
FOREIGN KEY ("bank_account_id")
REFERENCES "bank_accounts"("id")
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE "banking_transactions"
DROP CONSTRAINT IF EXISTS fk_bankingtransaction_csvfile,
ADD CONSTRAINT fk_bankingtransaction_csvfile
FOREIGN KEY ("csv_file_id")
REFERENCES "csv_files"("id")
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE "banking_transactions"
DROP CONSTRAINT IF EXISTS fk_bankingtransaction_bankaccount,
ADD CONSTRAINT fk_bankingtransaction_bankaccount
FOREIGN KEY ("bank_account_id")
REFERENCES "bank_accounts"("id")
ON UPDATE CASCADE
ON DELETE CASCADE;

-- email: test.user.projet.gbp@gmail.com  mot de passe: Admin123! --
INSERT INTO public.customers(creation_date, update_date, email, first_name, last_name, password, role)
	VALUES ('2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'test.user.projet.gbp@gmail.com', 'martin', 'dupont', '$2a$10$0x2.odzhf5409KeLIN6r/uii3g3C2hoDckIAgtjSY.XmbD4sXQGui', 'user');

INSERT INTO public.bank_accounts(initial_balance, creation_date, customer_id, reference_account, update_date, address, bank_name, city, type_account)
	VALUES ('0', '2023-09-04 16:30:00.000', '1', '15666777444', '2023-09-04 16:30:00.000', '12, allée du chataignet','Société Générale', 'Toulouse', 'Compte_courant');
INSERT INTO public.bank_accounts(initial_balance, creation_date, customer_id, reference_account, update_date, address, bank_name, city, type_account)
	VALUES ('500', '2023-09-04 16:30:00.000', '1', '17888555111', '2023-09-04 16:30:00.000', '17, rue jean jaures', 'BNP', 'Bordeaux', 'Livret_A');
INSERT INTO public.bank_accounts(initial_balance, creation_date, customer_id, reference_account, update_date, address, bank_name, city, type_account)
	VALUES ('230', '2023-09-04 16:30:00.000', '1', '25667745621', '2023-09-04 16:30:00.000', '59, boulevard voltaire', 'Cayman National', 'Savannah', 'Compte_joint');
INSERT INTO public.bank_accounts(initial_balance, creation_date, customer_id, reference_account, update_date, address, bank_name, city, type_account)
	VALUES ('600', '2023-09-04 16:30:00.000', '1', '56567489321', '2023-09-04 16:30:00.000', '106, avenue de la république','UBS', 'Zurich', 'PEE');
INSERT INTO public.bank_accounts(initial_balance, creation_date, customer_id, reference_account, update_date, address, bank_name, city, type_account)
	VALUES ('.20', '2023-09-04 16:30:00.000', '1', '45632078411', '2023-09-04 16:30:00.000', '20, chemin de fer','Barclays', 'Londres', 'Assurance_vie');

INSERT INTO public.csv_files(bank_account_id, creation_date, update_date, file_name)
	VALUES ('1', '2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'Relevé_oct2022.xlsx');
INSERT INTO public.csv_files(bank_account_id, creation_date, update_date, file_name)
	VALUES ('1', '2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'Relevé_nov2022.xlsx');
INSERT INTO public.csv_files(bank_account_id, creation_date, update_date, file_name)
	VALUES ('1', '2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'Relevé_dec2022.xlsx');
INSERT INTO public.csv_files(bank_account_id, creation_date, update_date, file_name)
	VALUES ('2', '2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'Relevé_dec2022.xlsx');
INSERT INTO public.csv_files(bank_account_id, creation_date, update_date, file_name)
    VALUES ('2', '2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'Relevé_nov2022.xlsx');
INSERT INTO public.csv_files(bank_account_id, creation_date, update_date, file_name)
    VALUES ('3', '2023-09-04 16:30:00.000', '2023-09-04 16:30:00.000', 'Relevé_dec2022.xlsx');

INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
	VALUES (null, '2022-10-10', '120.00', '1', '2023-09-04 16:30:00.000', '1', '4', '2023-09-04 16:30:00.000', 'Essence Intermarché la rive droite');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
	VALUES (null, '2022-10-10', '368.45', '1', '2023-09-04 16:30:00.000', '1', '7', '2023-09-04 16:30:00.000', 'Course Intermarché la rive droite');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-12', '345.51', '1', '2023-09-04 16:30:00.000', '1', '14', '2023-09-04 16:30:00.000', 'Garage, on vous dépanne');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-13', '1.10', '1', '2023-09-04 16:30:00.000', '1', '7', '2023-09-04 16:30:00.000', 'Boulangerie, le bon pain');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-14', '263.22', '1', '2023-09-04 16:30:00.000', '1', '2', '2023-09-04 16:30:00.000', 'Eau - Toulouse Métrople');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-14', '923.45', '1', '2023-09-04 16:30:00.000', '1', '5', '2023-09-04 16:30:00.000', 'Impôt foncier');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-14', '1223.41', '1', '2023-09-04 16:30:00.000', '1', '5', '2023-09-04 16:30:00.000', 'Impôt sur le revenue');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-17', '69.76', '1', '2023-09-04 16:30:00.000', '1', '8', '2023-09-04 16:30:00.000', 'Jules centre commercial la flêche blanche');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-10-17', '367.41', '1', '2023-09-04 16:30:00.000', '1', '3', '2023-09-04 16:30:00.000', 'Electicité, mensualité oct');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('2156.34', '2022-10-24', null, '1', '2023-09-04 16:30:00.000', '1', '23', '2023-09-04 16:30:00.000', 'Kiloutou, Salaire sept');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('24.56', '2022-10-24', null, '1', '2023-09-04 16:30:00.000', '1', '25', '2023-09-04 16:30:00.000', 'Remboursement santé Médecin');


INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('200.00', '2022-06-24', null, '2', '2023-09-04 16:30:00.000', '4', '24', '2023-09-04 16:30:00.000', 'Epargne espèce Juin-24');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('200.00', '2022-07-24', null, '2', '2023-09-04 16:30:00.000', '4', '24', '2023-09-04 16:30:00.000', 'Epargne espèce Juillet-24');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('200.00', '2022-08-24', null, '2', '2023-09-04 16:30:00.000', '4', '24', '2023-09-04 16:30:00.000', 'Epargne espèce Août-24');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('200.00', '2022-09-24', null, '2', '2023-09-04 16:30:00.000', '4', '24', '2023-09-04 16:30:00.000', 'Epargne espèce Sept-24');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('200.00', '2022-10-24', null, '2', '2023-09-04 16:30:00.000', '4', '24', '2023-09-04 16:30:00.000', 'Epargne espèce Oct-24');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-11-01', '600.00', '2', '2023-09-04 16:30:00.000', '4', '24', '2023-09-04 16:30:00.000', 'Epargne espèce Oct-24');


INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('1500.00', '2022-11-15', null, '3', '2023-09-04 16:30:00.000', '6', '24', '2023-09-04 16:30:00.000', 'Virement Mme.Dupont');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES ('1500.00', '2022-11-15', null, '3', '2023-09-04 16:30:00.000', '6', '24', '2023-09-04 16:30:00.000', 'Virement M.Dupont');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-11-16', '256.24', '3', '2023-09-04 16:30:00.000', '6', '7', '2023-09-04 16:30:00.000', 'Course Carrefour - rond point fleurie');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-11-18', '550.00', '3', '2023-09-04 16:30:00.000', '6', '6', '2023-09-04 16:30:00.000', 'Réservation location Biarritz');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-11-20', '1200.00', '3', '2023-09-04 16:30:00.000', '6', '19', '2023-09-04 16:30:00.000', 'Remboursement Maison');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-11-24', '428.62', '3', '2023-09-04 16:30:00.000', '6', '14', '2023-09-04 16:30:00.000', 'Billet avion Toulouse-Biarritz');
INSERT INTO public.banking_transactions(credit, date, debit, bank_account_id, creation_date, csv_file_id, category_id, update_date, description)
    VALUES (null, '2022-11-26', '154.22', '3', '2023-09-04 16:30:00.000', '6', '7', '2023-09-04 16:30:00.000', 'Course Carrefour - rond point fleurie');